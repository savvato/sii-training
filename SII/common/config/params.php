<?php
return [
    'adminEmail' => 'savvato@outlook.com',
    'supportEmail' => 'savvato@outlook.com',
    'user.passwordResetTokenExpire' => 3600,
];
