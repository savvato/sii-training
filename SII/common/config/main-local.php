<?php
return [
    'components' => [
        /*'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=db_sii',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',

            //Кеширование схемы БД
            //'enableSchemaCashe' => false, //TODO: при развертывании сменить на true и раскомментировать следующие 2 строки
            //'schemaCashe' => 'cashe',
            //'schemaCacheDuration' => 3600,
        ],*/
        'db' => null,
        'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            'dsn' => 'mongodb://sii:hGDI45@localhost/sii',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
