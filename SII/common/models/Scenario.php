<?php
/**
 * Created by PhpStorm.
 * User: savva
 * Date: 01.11.2015
 * Time: 16:05
 */

namespace common\models;

use yii\mongodb\ActiveRecord;

class Scenario extends ActiveRecord
{
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'scenarios';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'categoryID', 'settings', 'manual_scenario'];
    }

    public static function createEmptyScenario($category)
    {
        $scenario = new Scenario;
        $scenario->categoryID = $category;
        $scenario->manual_scenario = [];
        $scenario->settings =
            [
                "hasVideo" => false,
                "video" => null,
                "hasSidebarBackground" => false,
                "sidebarBackground" => null,
                "status" => "NotAvailable"
            ];
        return $scenario->save();
    }

    public static function deleteScenario($category)
    {
        $scenario = Scenario::find()->where(['categoryID' => $category])->one();
        return $scenario->delete();
    }
}