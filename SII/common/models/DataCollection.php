<?php
namespace common\models;

use yii\mongodb\ActiveRecord;

class DataCollection extends ActiveRecord
{
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'data_collections';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'collection_name', 'settings', 'structure', 'content'];
    }

    /**
     * @param $id
     * @return bool|int
     * Функция удаления коллекции
     */
    public static function deleteCollection($id)
    {
        $collection = DataCollection::find()->where(['_id' => $id])->one();
        return $collection->delete();
    }
}