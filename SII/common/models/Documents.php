<?php

namespace common\models;

use Yii;
use yii\mongodb\ActiveRecord;

/**
 * This is the model class for table "documents".
 *
 * @property integer $id
 * @property string $short_name
 * @property string $full_name
 * @property string $url
 */
class Documents extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'documents';
    }

    public function attributes()
    {
        return
            [
                '_id',
                'id',
                'short_name',
                'full_name',
                'url',
                'extension'
            ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['short_name', 'full_name', 'url', 'extension'], 'string'],
            [['url'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_name' => 'Краткое наименование',
            'full_name' => 'Полное наименование',
            'url' => 'URL',
            'extension' => 'Расширение файла документа'
        ];
    }
}
