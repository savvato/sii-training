<?php

namespace common\models;

use Yii;
use yii\mongodb\ActiveRecord;


/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property string $default
 */
class Settings extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'settings';
    }

    public function attributes()
    {
        return ['_id', 'key', 'value', 'default'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['value', 'default'], 'string'],
            [['key'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
            'default' => 'Default',
        ];
    }

    public static function getSiteStatus()
    {
        $status = Settings::find()->where(['key' => 'site_status'])->one();
        return $status->value ? $status->value : $status->default;
    }

    public static function setSiteStatus($value)
    {
        $status = Settings::find()->where(['key' => 'site_status'])->one();
        $status->value = $value;
        $status->save();
    }

    /**
     * Выгрузка имени сайта
     * @return mixed
     */
    public static function getSiteName()
    {
        $siteName = Settings::find()->where(['key' => 'site_name'])->one();
        return $siteName->value;
    }

    /**
     * Установка имени сайта
     * @param $name
     */
    public static function setSiteName($name)
    {
        $siteName = Settings::find()->where(['key' => 'site_name'])->one();
        $siteName->value = $name;
        return $siteName->save();
    }

    /**
     * Выгрузка страницы приветствия
     * @return mixed
     */
    public static function getHelloPage()
    {
        $page = Settings::find()->where(['key' => 'hello_page'])->one();
        return $page->value ? $page->value : $page->default;
    }

    /**
     * Установка страницы приветствия
     * @param $value - HTML-код страницы
     */
    public static function setHelloPage($value)
    {
        $page = Settings::find()->where(['key' => 'hello_page'])->one();
        $page->value = $value;
        return $page->save();
    }

    /**
     * Выгрузка страницы с контактами
     * @return mixed
     */
    public static function getContactsPage()
    {
        $page = Settings::find()->where(['key' => 'contacts_page'])->one();
        return $page->value ? $page->value : $page->default;
    }

    /**
     * Установка страницы с контактами
     * @param $value - HTML-код страницы
     */
    public static function setContactsPage($value)
    {
        $page = Settings::find()->where(['key' => 'contacts_page'])->one();
        $page->value = $value;
        return $page->save();
    }

    public static function setLogo($path)
    {
        $path = strtr($path, ['..' => '.']);
        $logo = Settings::find()->where(['key' => 'logo'])->one();
        $logo->value = $path;
        return $logo->save();
    }

    public static function getLogo()
    {
        $logo = Settings::find()->where(['key' => 'logo'])->one();
        return $logo->value;
    }

    public static function getSiteAddress()
    {
        $address = Settings::find()->where(['key' => 'site_address'])->one();
        return $address->value ? $address->value : $address->default;
    }

    public static function setSiteAddress($siteAddress)
    {
        $address = Settings::find()->where(['key' => 'site_address'])->one();
        $address->value = $siteAddress;
        $address->save();
    }

    public static function setHelpPage($value)
    {
        $page = Settings::find()->where(['key' => 'help_page'])->one();
        $page->value = $value;
        return $page->save();
    }

    public static function getHelpPage()
    {
        $page = Settings::find()->where(['key' => 'help_page'])->one();
        return $page->value;
    }

    public static function setPartnershipPage($value)
    {
        $page = Settings::find()->where(['key' => 'partnership_page'])->one();
        $page->value = $value;
        return $page->save();
    }

    public static function getPartnershipPage()
    {
        $page = Settings::find()->where(['key' => 'partnership_page'])->one();
        return $page->value;
    }
}
