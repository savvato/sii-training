<?php

namespace common\models;

use Yii;
use yii\mongodb\ActiveRecord;

/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 */
class Video extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'videos';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function attributes()
    {
        return ['_id', 'id', 'name', 'code'];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
        ];
    }
}
