<?php

namespace common\models;

use Yii;
use yii\mongodb\ActiveRecord;
/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $subject
 * @property string $body
 * @property string $date
 * @property integer $is_readed
 * @property integer $category_id
 *
 * @property BusinessEnvironment $category
 */
class Feedback extends ActiveRecord
{
    public static function primaryKey()
    {
        return ['id'];
    }

    public static function collectionName()
    {
        return 'feedback';
    }

    public function attributes()
    {
        return
            [
                '_id',
                'id',
                'name',
                'email',
                'subject',
                'body',
                'date',
                'is_readed',
                'category'
            ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'subject', 'body', 'date', 'category'], 'required'],
            [['name', 'email', 'subject', 'body', 'category'], 'string'],
            [['date'], 'safe'],
            [['is_readed'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Ф.И.О.',
            'email' => 'E-mail',
            'subject' => 'Тема сообщения',
            'body' => 'Сообщение',
            'date' => 'Дата сообщения',
            'is_readed' => 'Is Readed',
            'category' => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getCategory()
    {
        return $this->hasOne(BusinessEnvironment::className(), ['id' => 'category_id']);
    }*/

    public static function getCountOfUnreadMessages()
    {
        return Feedback::find()->where(['is_readed' => false])->count();
    }
}
