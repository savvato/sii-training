<?php

namespace common\models;

use Yii;
use yii\mongodb\ActiveRecord;
/**
 * This is the model class for table "business_environment".
 *
 * @property integer $id
 * @property string $name
 * @property integer $main
 */
class BusinessEnvironment extends ActiveRecord
{
    public static function primaryKey()
    {
        return ['id'];
    }

    public static function collectionName()
    {
        return 'business_environment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['main'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    public function attributes()
    {
        return ['_id', 'id', 'name', 'main'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'main' => 'Статус',
        ];
    }

    public static function getMainCategories()
    {
        return BusinessEnvironment::find()->where(['main' => 1])->all();
    }

    public static function getAdvancedCategories(){
        return BusinessEnvironment::find()->where(['main' => 0])->all();
    }

    public static function getCategoryName($id)
    {
        $category = BusinessEnvironment::find()->where(['id' => $id])->one();
        return $category->name;
    }
}
