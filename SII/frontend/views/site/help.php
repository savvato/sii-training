<?php
use common\models\Settings;
$this->title = 'Помощь';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Помощь',
        'url' => ['site/help'],
        'linkOptions' => ['style' => 'color: ##3E71CF;']
    ]
]
?>
<div class="container-fluid">
    <h1>Помощь</h1>
    <?= Settings::getHelpPage() ?>
</div>
