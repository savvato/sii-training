<?php
use yii\helpers\Url;
$this->title = 'Документы';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Документы',
        'url' => ['site/documents'],
        'linkOptions' => ['style' => 'color: ##3E71CF;']
    ]
]
?>
<div class="container-fluid well">
    <h1>Документы</h1>
    <?php foreach($documents as $document): ?>
        <?php
        if($document->full_name):
        ?>
            <a
                href="<?= Url::to([$document->url]) ?>"
                class="btn btn-default"
                style="width: 100%; text-align: left; font-size: 22px;"
                target="_blank">
                <span class="glyphicon glyphicon-file"></span><?= $document->full_name ?>
            </a>
            <br>
            <br>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
