<?php
use common\models\Settings;
$this->title = 'Партнерство';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Партнерство',
        'url' => ['site/partnership'],
        'linkOptions' => ['style' => 'color: ##3E71CF;']
    ]
]
?>
<div class="container-fluid">
    <h1>Партнерство</h1>
    <?= Settings::getPartnershipPage() ?>
</div>
