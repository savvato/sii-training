<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = \common\models\Settings::getSiteName();
?>
<div class="site-index">
    <div class="container jumbotron">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <?= $hello ?>

                <?php
                    if (count($mainCategories) != 0 || count($advancedCategories) != 0):
                ?>
                <p class="lead">
                    для вашего удобства мы предлагаем вам выбрать интересующую вас группу пользователей
                </p>
                <?php
                    endif;
                ?>
                <div class="btn-container">
                    <?php
                        foreach($mainCategories as $category):
                    ?>
                        <a href="<?= Url::to(['scenario/mode', 'id' => $category->id]) ?>">
                            <button class="btn btn-lg btn-category"><?= mb_strtoupper($category->name) ?></button>
                        </a>
                    <?php endforeach; ?>
                </div>
                <?php
                    if (count($advancedCategories) != 0):
                ?>
                <div href="#advanced-categories" data-toggle="collapse" id="show-categories" class="spoiler collapsed">
                    <strike class="hidden-xs">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</strike>
                    <small> Просмотреть все группы</small>
                    <button class="btn btn-default" style="padding: 6px 8px 1px 10px; border-radius: 20px; border-color: #3E70BA;">
                        <span id="arrow" class="glyphicon glyphicon-chevron-down"></span>
                    </button>
                    <strike class="hidden-xs">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</strike>
                </div>
                <div class="collapse" id="advanced-categories">
                    <div class="btn-container">
                        <?php foreach ($advancedCategories as $category): ?>
                            <a href="<?= Url::to(['scenario/mode', 'id' => $category->id]) ?>">
                                <button class="btn btn-lg btn-category">
                                    <?= mb_strtoupper($category->name) ?>
                                </button>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php
                    endif;
                ?>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
