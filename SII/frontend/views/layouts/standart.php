<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\models\Settings;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => "<img src='".Url::to([Settings::getLogo()])."' style='height: 40px; margin-top: 5px; margin-bottom: 5px;'>",
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-static-top',
        ],
    ]);
    $menuItems = [
        ['label' =>  'Контакты', 'url' => ['/site/contacts']],
        ['label' => 'Помощь', 'url' => ['/site/help']],
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right', 'style' => 'font-weight: bold; font-size: 18px;'],
        'items' => $menuItems,
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right', 'style' => 'font-weight: bold; font-size: 18px;'],
        'encodeLabels' => false,
        'items' =>  [
            [
                'label' => Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'homeLink' => false
                ]),
                'options' => ['class' => 'hidden-xs'],
                'linkOptions' => [
                    'style' => 'font-weight: bold; font-size: 16px; display: none;'
                ],
            ],
            [
                'label' =>  '|',
                'options' => ['class' => 'hidden-xs'],
                'linkOptions' => ['style' => 'font-size: 24px; font-weight: lighter;'],

            ],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">

        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar-default',
        ],
    ]);
    $menuItems = [
        ['label' =>  'Партнерство', 'url' => ['/site/partnership']],
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left', 'style' => 'font-weight: bold; font-size: 18px;'],
        'items' => $menuItems,
    ]);
    $menuItems = [
        ['label' =>  'Связаться с нами', 'url' => ['/site/feedback']],
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right', 'style' => 'font-weight: bold; font-size: 18px;'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
</footer>
<!-- Piwik -->
<script type="text/javascript">
    var _paq = _paq || [];
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="<?= Url::base() ?>/control/analytics/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', 1]);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript>
    <p>
        <img src="<?= Url::base() ?>/control/analytics/piwik.php?idsite=1" style="border:0;" alt="" />
    </p>
</noscript>
<!-- End Piwik Code -->
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
