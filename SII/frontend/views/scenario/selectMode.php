<?php
use yii\helpers\Url;
$this->title = Yii::$app->session->get('category');
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::$app->session->get('category'),
        'url' => ['scenario/mode', 'id' => $categoryID],
        'linkOptions' => ['style' => 'color: ##3E71CF;']
    ]
]
?>
<div class=" select-page container jumbotron">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="container">
                <p class="lead">ДЛЯ ЭКОНОМИИ ВРЕМЕНИ ВЫ МОЖЕТЕ ВЫБРАТЬ АВТОМАТИЧЕСКИЙ РЕЖИМ ПРОСМОТРА</p>
                <a href="<?= Url::to(['scenario/video']) ?>" style="width: 70%;">
                    <button style="width: 70%;" class="btn btn-lg btn-category">ПОСМОТРЕТЬ ВИДЕО <span class='glyphicon glyphicon-play-circle'></span></button>
                </a>
            </div>
            <div class="container" style="margin-top: 30px">
                <p class="lead">ИЛИ УЗНАТЬ О НАС БОЛЕЕ ДЕТАЛЬНО ИЗУЧИВ СОДЕРЖИМОЕ САЙТА САМОСТОЯТЕЛЬНО</p>
                <a href="<?= Url::to(['scenario/content', 'groupID' => 0, 'stepID' => 0]) ?>" style="width: 70%;">
                    <button style="width: 70%;" class="btn btn-lg btn-category">ХОЧУ УЗНАТЬ ВСЁ ПОДРОБНО</button>
                </a>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
