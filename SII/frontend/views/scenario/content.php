<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

$this->title = $currentPage['pageName'];
$session = Yii::$app->session;
$categoryID = (int)$session->get('categoryID');
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::$app->session->get('category'),
        'url' => ['scenario/mode', 'id' => $categoryID],
        'linkOptions' => ['style' => 'color: #999999;']
    ],
    [
        'label' => $currentPage['pageName'],
        'url' => ['scenario/content', 'groupID' => $currentPage['groupID'], 'stepID' => $currentPage['stepID']],
        'linkOptions' => ['style' => 'color: black;']
    ]
]
?>
<div class="page not-margins" style="position: relative">
    <div class="sidebar not-paddings">
        <?php
        $menuItems = [];
        for ($groupID = 0; $groupID <= count($scenario) - 1; $groupID++)
        {
            $menuItems[] = '<li class="group"><p class="group-link">'. $scenario[$groupID]['group_name'].'</p></li>';

            $steps = $scenario[$groupID]['steps'];
            for ($stepID = 0; $stepID <= count($steps) - 1; $stepID++)
            {
                $menuItems[] = [
                    'label' => mb_strtoupper($steps[$stepID]['step_name']),
                    'url' => ['/scenario/content', 'groupID' => $groupID, 'stepID' => $stepID],
                    'linkOptions' => [
                        'class' => 'step-link'
                    ],
                ];
            }
        }
        $backgroundStyle = null;
        if ($scenarioSettings['hasSidebarBackground'])
        {
            $backgroundImage = $scenarioSettings['sidebarBackground']['url'];
            $backgroundImage = Url::to([strtr($backgroundImage, ['..' => '.'])]);
            $backgroundStyle = 'background-image: url("'.$backgroundImage.'")';
        }
        else
        {
            $backgroundStyle = 'background-color: #787878';
        }

        echo Nav::widget([
            'options' =>
                [
                    'class' => 'nav-pills nav-stacked',
                    'style' => $backgroundStyle
                ],
            'items' => $menuItems,
        ]);
        ?>
    </div>

    <div class="content not-paddings" style="position: relative;">

        <?= $content ?>
    </div>
</div>
<div class="icon-menu" style="position: absolute; top: 0px; padding-left: 5px; z-index:10000; cursor: pointer; ">
    <img src="<?= Url::to(['./appImages/el_24.png']) ?>" style="height: 55px;">
</div>


