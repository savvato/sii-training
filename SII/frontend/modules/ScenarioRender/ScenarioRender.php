<?php

namespace frontend\modules\ScenarioRender;

use frontend\models\ContactForm;
use frontend\modules\ScenarioRender\controllers\RenderController;
use Yii;

class ScenarioRender extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\ScenarioRender\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public static function render($step, $category, $currentPage)
    {
        $result = null;
        switch ($step['type'])
        {
            case 'simplePage':
                $result = ScenarioRender::renderSimplePage($step);
                break;
            case 'sliderPage':
                $result = ScenarioRender::renderSliderPage($step);
                break;
            case 'feedbackPage':
                $result = ScenarioRender::renderFeedbackPage($step, $category, $currentPage);
                break;
            case 'documentPage':
                $result = ScenarioRender::renderDocumentPage($step);
                break;
        }
        return $result;
    }

    private static function renderSimplePage($step)
    {
        $contentStructure = $step['content_structure'];
        $page = '';
        foreach($contentStructure as $section)
        {
            $page .= RenderController::renderSection($section);
        }
        return $page;
    }

    private static function renderSliderPage($step)
    {
        $slideBlocks = '';
        $slides = '';
        foreach ($step['content_structure'] as $index => $slide)
        {
            $slideBlocks .= RenderController::renderDefinitiveBlock($slide['block'], $index);
            $slideContent = ScenarioRender::renderSimplePage($slide);
            $slides .= RenderController::renderSlide($slideContent, $index);
        }

        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/sliderPage',
            [
                'blocks' => $slideBlocks,
                'slides' => $slides
            ]
        );
    }

    private static function renderFeedbackPage($step, $category, $currentPage)
    {
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/feedbackForm',
            [
                'category' => $category,
                'content' => $step['content_structure'],
                'model' => new ContactForm(),
                'currentPage' => $currentPage
            ]
        );
    }

    private static function renderDocumentPage($step)
    {
        return ScenarioRender::renderSliderPage($step);
    }
}
