<?php
use yii\helpers\Url;
?>

<div class="container-fluid">
    <div class="docs-container">
        <?php
            foreach($documents as $document):
        ?>
                <?php
                    if ($document['extension'] == 'doc' || $document['extension'] == 'docx')
                    {
                        $extStyle = 'doc';
                    }
                    else if ($document['extension'] == 'pdf')
                    {
                        $extStyle = 'pdf';
                    }
                    else if ($document['extension'] == 'xls' || $document['extension'] == 'xlsx')
                    {
                        $extStyle = 'xls';
                    }
                ?>
                <a
                     href="<?= Url::to([$document['url']]) ?>"
                     target="_blank"
                     class="doc col-md-4">
                     <span class="doc_<?= $extStyle ?>"><?= mb_strtoupper($extStyle) ?></span>
                     <span class="doc-name"><?= $document['full_name'] ?></span>
                </a>

        <?php
            endforeach;
        ?>
    </div>
</div>
