<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\BusinessEnvironment;
use yii\captcha\Captcha;
?>
<div class="container-fluid" style="display: flex">
    <div class="col-lg-5" style="margin: auto">
        <div>
            <?= $content['introduction'] ?>
        </div>
        <?php $form = ActiveForm::begin(
            [
                'id' => 'contact-form',
                'action' => ['scenario/feedback', 'groupID' => $currentPage['groupID'], 'stepID' => $currentPage['stepID']],
                'method' => 'post'
            ]); ?>
        <?= $form->field($model, 'category', ['enableLabel' => false])->hiddenInput(['value' => $category]) ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'subject') ?>
        <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
            'template' => '<div class="row"><div class="col-lg-4">{image}</div><div class="col-lg-6">{input}</div></div>',
        ]) ?>
        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

