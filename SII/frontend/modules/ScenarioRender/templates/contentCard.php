<?php
use yii\helpers\Url;
?>
<div class="content-card has-blocks col-md-<?= $settings['blockWidth'] ?>">
    <div class="container-fluid">
        <div class="row">
            <?php
                if ($settings['imagePlace'] == 'top'):
            ?>
            <div <?= $sectionSettings['hasBorders'] == true ? 'class="bordered"' : '' ?>>
                <img src="<?= Url::to([strtr($settings['image']['url'], ['..' => '.'])]) ?>" class="img-responsive">
                <div><?= $content['title'] ?></div>
                <div><?= $content['text'] ?></div>
            </div>
            <?php endif; ?>
            <?php
                if ($settings['imagePlace'] == 'bottom'):
            ?>
                <div <?= $sectionSettings['hasBorders'] == true ? 'class="bordered"' : '' ?>>
                    <div><?= $content['title'] ?></div>
                    <div><?= $content['text'] ?></div>
                    <img src="<?= Url::to([strtr($settings['image']['url'], ['..' => '.'])]) ?>" class="img-responsive">
                </div>
            <?php endif; ?>
            <?php
                if ($settings['imagePlace'] == 'left'):
            ?>
                <div <?= $sectionSettings['hasBorders'] == true ? 'class="bordered"' : '' ?>>
                    <div class="col-md-<?= $settings['imageWidth'] ?>">
                        <img src="<?= Url::to([strtr($settings['image']['url'], ['..' => '.'])]) ?>" class="img-responsive">
                    </div>
                    <div class="col-md-<?= $settings['contentWidth'] ?>">
                        <div><?= $content['title'] ?></div>
                        <div><?= $content['text'] ?></div>
                    </div>
                </div>
            <?php endif; ?>

            <?php
                if ($settings['imagePlace'] == 'right'):
            ?>
                <div <?= $sectionSettings['hasBorders'] == true ? 'class="bordered"' : '' ?>>
                    <div class="col-md-<?= $settings['contentWidth'] ?>">
                        <div><?= $content['title'] ?></div>
                        <div><?= $content['text'] ?></div>
                    </div>
                    <div class="col-md-<?= $settings['imageWidth'] ?>">
                        <img src="<?= Url::to([strtr($settings['image']['url'], ['..' => '.'])]) ?>" class="img-responsive">
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>