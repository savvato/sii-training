<?php
use yii\helpers\Url;
?>

<div class="trick col-md-<?= $settings['width'] ?>">
    <div class="trick-main">
        <img
            src="<?= Url::to([strtr($settings['backgroundImage']['url'], ['..' => '.'])]) ?>"
            class="img-responsive">
        <div class="trick-text-content">
            <h3  style="color: <?= $settings['fontColor'] ?>">
                <?= $content['title'] ?>
            </h3>
        </div>
    </div>
    <div class="trick-content" style="background-color: <?= $settings['annotationBackgroundColor'] ?>;">
        <p style="padding: 10px;"><?= $settings['annotation'] ?></p>
    </div>
</div>
