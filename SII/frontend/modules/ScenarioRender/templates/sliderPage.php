<div class="header">
    <div class="cards container-fluid">
        <?= $blocks ?>
    </div>
</div>
<div id="main-carousel" class="carousel slide">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?= $slides ?>
    </div>
</div>