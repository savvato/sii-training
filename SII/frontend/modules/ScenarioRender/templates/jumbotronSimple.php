<?php
use yii\helpers\Url;
?>
<div class="jumbo">
    <img src="<?= Url::to([strtr($settings['backgroundImage']['url'], ['..' => '.'])]) ?>" style="width: 100%;">
    <div class="jumbo-content col-md-2" >
        <div>
            <div><?= $content['title'] ?></div>
            <div><?= $content['text'] ?></div>
        </div>
    </div>
</div>