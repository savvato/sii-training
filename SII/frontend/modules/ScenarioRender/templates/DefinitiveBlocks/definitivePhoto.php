<?php
use yii\helpers\Url;
?>
<div class="col-md-3"
     href="#main-carousel"
     data-slide-to="<?= $index ?>">
    <div style="position: relative;">
        <img src="<?= Url::to([strtr($settings['main_image']['url'], ['..' => '.'])]) ?>" class="img-responsive" style="height: 200px;">
        <div style="position: absolute; top: 0; left: 0;  display: flex; width: 100%; height: 100%">
            <h3 style="color: white; margin: auto; z-index: 100; text-align: center; ">
                <?= $content['title'] ?>
            </h3>
        </div>
    </div>
</div>
