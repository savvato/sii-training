<?php
use yii\helpers\Url;
?>
<div class="col-md-3"
     href="#main-carousel"
     data-slide-to="<?= $index ?>">
    <div class="card">
        <img src="<?= Url::to([strtr($settings['main_image']['url'], ['..' => '.'])]) ?>" style="width: 100%">
        <img class="pull-right" style="margin-top: 10px; margin-right: 5px" src="<?= Url::to([strtr($settings['image']['url'], ['..' => '.'])]) ?>"/>
        <h4><?= $content['title'] ?></h4>
        <h4><?= $content['second_title'] ?></h4>

        <p><?= $content['text'] ?></p>
        <small><?= $content['clarification'] ?></small>
    </div>
</div>
