<?php
use yii\helpers\Url;
?>
<div class="jumbo">
    <img src="<?= Url::to([strtr($settings['backgroundImage']['url'], ['..' => '.'])]) ?>" style="width: 100%;">
    <div class="multiple-content jumbo-multiple">
        <?= $blocks ?>
    </div>
</div>