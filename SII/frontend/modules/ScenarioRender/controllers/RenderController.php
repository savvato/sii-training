<?php

namespace frontend\modules\ScenarioRender\controllers;

use Yii;
use common\models\Documents;
use common\models\DataCollection;

class RenderController
{
    public static function renderSection($section)
    {
        $result = null;
        switch ($section['type'])
        {
            case 'separator':
                $result = RenderController::renderSeparator($section);
                break;
            case 'tricks':
                $result = RenderController::renderTricks($section);
                break;
            case 'jumbotronSimple':
                $result = RenderController::renderSimpleJumbotron($section);
                break;
            case 'jumbotronMultiple':
                $result = RenderController::renderMultipleJumbotron($section);
                break;
            case 'contentCards':
                $result = RenderController::renderContentCards($section);
                break;
            case 'documentsSection':
                $result = RenderController::renderDocumentsSection($section);
                break;
        }
        return $result;
    }

    private static function renderSeparator($section)
    {
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/separator',
            [
                'content' => $section['content']
            ]
        );
    }

    private static function renderTricks($section)
    {
        $content = '';
        $blocks = $section['blocks'];
        foreach($blocks as $block)
        {
            $content .= RenderController::renderTrick($block, $section['settings']);
        }
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/tricks',
            [
                'content' => $content,
                'settings'=> $section['settings']
            ]
        );
    }

    private static function renderTrick($block, $sectionSettings)
    {
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/trick',
            [
                'content' => $block['content'],
                'settings'=> $block['settings'],
                'sectionSettings' => $sectionSettings
            ]
        );
    }

    private static function renderSimpleJumbotron($section)
    {
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/jumbotronSimple',
            [
                'content' => $section['content'],
                'settings'=> $section['settings']
            ]
        );
    }

    private static function renderMultipleJumbotron($section)
    {
        $blocksHtml = '';
        if ($section['settings']['hasIntegration'])
        {
            $integrationSettings = $section['settings']['integrationSettings'];
            $collection = DataCollection::find()->where(['collection_name' => $integrationSettings['collectionName']])->one();
            foreach ($collection['content'] as $record)
            {
                $content = [
                    'title' => $record[$integrationSettings['matchingFields']['title']],
                    'text' => $record[$integrationSettings['matchingFields']['text']]
                ];
                $blocksHtml .= RenderController::renderJumboBlock($content, 3);
            }
        }
        else
        {
            $blocks = $section['blocks'];
            foreach ($blocks as $block)
            {
                $blocksHtml .= RenderController::renderJumboBlock($block['content'], $block['settings']['width']);
            }
        }
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/jumbotronMultiple',
            [
                'blocks' => $blocksHtml,
                'settings'=> $section['settings']
            ]
        );
    }

    private static function renderJumboBlock($content, $width)
    {
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/jumboBlock',
            [
                'content' => $content,
                'width'=> $width
            ]
        );
    }

    public static function renderDefinitiveBlock($block, $index)
    {
        $result = null;
        switch ($block['type'])
        {
            case 'definitiveCard':
                $result = RenderController::renderDefinitiveCard($block, $index);
                break;
            case 'definitivePhotoCard':
                $result = RenderController::renderDefinitivePhotoCard($block, $index);
                break;
            case 'definitivePhoto':
                $result = RenderController::renderDefinitivePhoto($block, $index);
                break;
        }
        return $result;
    }

    private static function renderDefinitiveCard($block, $index)
    {
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/DefinitiveBlocks/definitiveCard',
            [
                'content' => $block['content'],
                'settings' => $block['settings'],
                'index'=> $index
            ]
        );
    }

    private static function renderDefinitivePhotoCard($block, $index)
    {
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/DefinitiveBlocks/definitivePhotoCard',
            [
                'content' => $block['content'],
                'settings' => $block['settings'],
                'index'=> $index
            ]
        );
    }

    private static function renderDefinitivePhoto($block, $index)
    {
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/DefinitiveBlocks/definitivePhoto',
            [
                'content' => $block['content'],
                'settings' => $block['settings'],
                'index'=> $index
            ]
        );
    }

    public static function renderSlide($content, $index)
    {
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/slide',
            [
                'content' => $content,
                'index' => $index
            ]
        );
    }

    public static function renderContentCards($section)
    {
        $blocks = $section['blocks'];
        $sectionSettings = $section['settings'];
        $content = '';
        foreach ($blocks as $block)
        {
            $content .= RenderController::renderContentCard($block, $sectionSettings);
        }
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/contentCards',
            [
                'content' => $content,
            ]
        );
    }

    private static function renderContentCard($block, $sectionSettings)
    {
        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/contentCard',
            [
                'content' => $block['content'],
                'settings' => $block['settings'],
                'sectionSettings' => $sectionSettings
            ]
        );
    }

    private static function renderDocumentsSection($section)
    {
        $idDocuments = $section['documents'];
        $documents = [];
        foreach ($idDocuments as $idDocument) {
            $document = Documents::find()->where(['id' => $idDocument['id']])->one();
            $documents[] = $document->toArray();
        }

        return Yii::$app->view->render(
            '@frontend/modules/ScenarioRender/templates/documentsSection',
            [
                'documents' => $documents
            ]
        );
    }
}