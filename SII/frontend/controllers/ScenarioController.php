<?php

namespace frontend\controllers;

use Yii;
use common\models\Video;
use yii\web\Controller;
use frontend\modules\ScenarioRender\ScenarioRender;
use common\models\Scenario;
use common\models\BusinessEnvironment;
use common\models\Feedback;
use common\models\Settings;
use frontend\models\ContactForm;

class ScenarioController extends Controller
{
    public function beforeAction($action)
    {
        if ($action->id != 'not-available')
        {
            if (Settings::getSiteStatus() == 'NotAvailable')
            {
                $this->redirect(['site/not-available']);
            }
        }
        return parent::beforeAction($action);
    }

    public function actionMode()
    {
        $request = Yii::$app->request;
        $categoryID = (int)$request->get('id');
        $category = BusinessEnvironment::find()->where(['id' => $categoryID])->one();
        $session = Yii::$app->session;
        $session->set('categoryID', $categoryID);
        $session->set('category', $category->name);

        $scenario = Scenario::find()->where(['categoryID' => $categoryID])->one();
        $settings = $scenario['settings'];
        if ($settings['status'] == 'NotAvailable')
        {
            $this->redirect(['content-error']);
        }
        else
        {
            if ($settings['hasVideo'])
            {
                return $this->render('selectMode', ['categoryID' => $categoryID]);
            }
            else
            {
                return $this->redirect(['content', 'groupID' => 0, 'stepID' => 0]);
            }
        }
    }

    public function actionContentError()
    {
        $session = Yii::$app->session;

        $name = $session->get('category');

        return $this->render('error', ['name' => $name]);
    }


    public function actionContent()
    {
        $session = Yii::$app->session;
        $categoryID = (int)$session->get('categoryID');
        $category = $session->get('category');
        $request = Yii::$app->request;
        $groupID = (int)$request->get('groupID');
        $stepID = (int)$request->get('stepID');

        $scenario = Scenario::find()->where(['categoryID' => $categoryID])->one();

        $manualScenario = $scenario['manual_scenario'];
        $group = $manualScenario[$groupID];
        $step = $group['steps'][$stepID];

        $currentPage = [
            'pageName' => $step['step_name'],
            'groupID' => $groupID,
            'stepID' => $stepID
        ];

        $scenarioSettings = $scenario['settings'];

        $content = ScenarioRender::render($step, $category, $currentPage);

        return $this->render('content',
            [
                'scenario' => $manualScenario,
                'content' => $content,
                'currentPage' => $currentPage,
                'scenarioSettings' => $scenarioSettings
            ]);
    }

    public function actionVideo()
    {
        $session = Yii::$app->session;
        $categoryID = (int)$session->get('categoryID');
        $scenario = Scenario::find()->where(['categoryID' => $categoryID])->one();
        if ($scenario['settings']['hasVideo'])
        {
            $videoID = (int)$scenario['settings']['videoID'];
            $video = Video::find()->where(['id' => $videoID])->one();
            if ($video)
            {
                return $this->render('video', ['video' => $video]);
            }
            else
            {
                return $this->redirect('error');
            }
        }
        else
        {
            return $this->redirect('error');
        }
    }

    public function actionFeedback()
    {
        $model = new ContactForm();
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $feedbackRecord = new Feedback();
                $feedbackRecord->id = Feedback::find()->count();
                $feedbackRecord->name = $model->name;
                $feedbackRecord->category = $model->category;
                $feedbackRecord->email = $model->email;
                $feedbackRecord->subject = $model->subject;
                $feedbackRecord->body = $model->body;
                $feedbackRecord->date = date('c');
                $feedbackRecord->is_readed = false;
                if ($feedbackRecord->save()) {
                    Yii::$app->session->setFlash('success', 'Спасибо за то, что вы связались с нами. Мы ответим вам сразу как это будет возможно.');
                } else {
                    Yii::$app->session->setFlash('error', 'Произошла ошибка.');
                }
                return $this->redirect(
                    [
                        'content',
                        'groupID' => Yii::$app->request->get('groupID'),
                        'stepID' => Yii::$app->request->get('stepID')
                    ]);
            }
        }
    }
}
