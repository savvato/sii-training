<?php
namespace frontend\controllers;

use common\models\BusinessEnvironment;
use common\models\Documents;
use common\models\Settings;
use common\models\Feedback;
use Yii;
use frontend\models\ContactForm;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function beforeAction($action)
    {
        if ($action->id != 'not-available')
        {
            if (Settings::getSiteStatus() == 'NotAvailable')
            {
                $this->redirect(['not-available']);
            }
        }
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'title';
        $hello = Settings::getHelloPage();
        $hello = strtr($hello, ['src="..' => 'src=".', 'alt="..' => 'alt=".', '<img' => '<img class="img-responsive"']);
        $mainCategories = BusinessEnvironment::getMainCategories();
        $advancedCategories = BusinessEnvironment::getAdvancedCategories();
        return $this->render('index',
            [
                'hello' => $hello,
                'mainCategories' => $mainCategories,
                'advancedCategories' => $advancedCategories
            ]);
    }

    public function actionNotAvailable()
    {
        $this->layout = 'empty';
        return $this->render('notAvailable');
    }

    public function actionFeedback()
    {
        $this->layout = 'standart';
        $model = new ContactForm();
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $feedbackRecord = new Feedback();
                $feedbackRecord->id = Feedback::find()->count();
                $feedbackRecord->name = $model->name;
                $feedbackRecord->category = $model->category;
                $feedbackRecord->email = $model->email;
                $feedbackRecord->subject = $model->subject;
                $feedbackRecord->body = $model->body;
                $feedbackRecord->date = date('c');
                $feedbackRecord->is_readed = false;
                if ($feedbackRecord->save()) {
                    Yii::$app->session->setFlash('success', 'Спасибо за то, что вы связались с нами. Мы ответим вам сразу как это будет возможно.');
                } else {
                    Yii::$app->session->setFlash('error', 'Произошла ошибка.');
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('feedback', [
                'model' => $model,
            ]);
        }
    }

    public function actionHelp()
    {
        $this->layout = 'standart';
        return $this->render('help');
    }

    /*public function actionDocuments()
    {
        $this->layout = 'standart';
        $documents = Documents::find()->all();
        return $this->render('documents', ['documents' => $documents]);
    }*/

    public function actionPartnership()
    {
        $this->layout = 'standart';
        return $this->render('partnership');
    }

    public function actionContacts()
    {
        $this->layout = 'standart';
        $page = Settings::getContactsPage();
        return $this->render('contacts', ['page' => $page]);
    }
}
