<?php

namespace backend\controllers;

use common\models\Video;
use Yii;
use yii\web\Controller;
use common\models\BusinessEnvironment;
use common\models\Scenario;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class ScenarioController extends Controller
{
    public $layout = 'scenarioEditor';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id === 'save' || $action->id === 'load') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /*
     * Вывод списка сценариев
     */
    public function actionIndex()
    {
        $this->layout = 'main';
        $scenarios = Scenario::find()->all();
        $categories = [];
        foreach($scenarios as $scenario)
        {
            $categories[] = [
                'id' => $scenario->categoryID,
                'name' => BusinessEnvironment::getCategoryName($scenario->categoryID)
            ];
        }
        ArrayHelper::multisort($categories, 'id', [SORT_ASC]);
        return $this->render('index', ['categories' => $categories]);
    }

    /*
     * Вывод редактора сценариев и контента
     */
    public function actionEditor()
    {
        $request = Yii::$app->request;
        $id = (int)$request->get('id');
        $session = Yii::$app->session;
        $session->set('categoryID', $id);
        return $this->render('scenario', [
            'id' => $id,
        ]);
    }

    /**
     * Выгразка сценария в редактор
     */
    public function actionLoad()
    {
        $session = Yii::$app->session;
        $id = $session->get('categoryID');
        $scenario = Scenario::find()->where(['categoryID' => $id])->one();
        $categoryName = BusinessEnvironment::getCategoryName($id);
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = ['categoryName' => $categoryName, 'scenario' => $scenario];
        $response->send();
    }

    /*
     * Сохранение сценария
     */
    public function actionSave()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $categoryID = $session->get('categoryID');
        $scenario = Scenario::find()->where(['categoryID' => $categoryID])->one();
        $manualScenario = $request->post('scenario');
        $settings = $request->post('settings');
        $manualScenario = Json::decode($manualScenario);
        $settings = Json::decode($settings);
        $scenario->manual_scenario = $manualScenario;
        $scenario->settings = $settings;
        if ($scenario->save())
        {
            Yii::$app->response->statusCode = 200;
        }
        else
        {
            Yii::$app->response->statusCode = 500;
        }

    }
}
