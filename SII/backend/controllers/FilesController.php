<?php

namespace backend\controllers;

use common\models\BusinessEnvironment;
use common\models\Scenario;
use common\models\Video;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use app\models\UploadImageForm;
use app\models\UploadDocumentForm;
use app\models\UploadVideoForm;
use yii\filters\AccessControl;
use common\models\Documents;

class FilesController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionImage()
    {
        return $this->render('images');
    }

    public function actionImagesLoad()
    {
        $model = new UploadImageForm();

        if (Yii::$app->request->isPost) {
            $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
            if ($model->upload()) {
                return $this->redirect(['image']);
            }
        }

        return $this->render('image', ['model' => $model]);
    }

    public function actionImageDelete()
    {
        $request = Yii::$app->request;
        $file = $request->get('file');
        unlink($file);
        Yii::$app->session->setFlash('success', 'Файл успешно удален.');
        return $this->actionImage();
    }

    public function actionVideo()
    {
        $videos = Video::find()->all();
        return $this->render('videos', ['videos' => $videos]);
    }

    public function actionVideoLoad()
    {
        $request = Yii::$app->request;
        if ($request->isGet)
        {
            return $this->render('video');
        }
        if ($request->isPost)
        {
            $video = new Video();
            if (Video::find()->count() == 0)
            {
                $video->id = 1;
            }
            else
            {
                $video->id = Video::find()->all()[Video::find()->count() - 1]->id + 1;
            }
            $video->name = $request->post('name');
            $video->code = $request->post('code');
            if ($video->save())
            {
                Yii::$app->session->setFlash('success', 'Видео успешно добавлено.');
                $this->redirect(['video']);
            }
        }
    }

    public function actionVideoUpdate()
    {
        $request = Yii::$app->request;
        $video = Video::find()->where(['id' => (int)$request->get('id')])->one();
        if ($request->isGet)
        {
            return $this->render('videoUpdateForm', ['video' => $video]);
        }
        if ($request->isPost)
        {
            $video->name = $request->post('name');
            $video->code = $request->post('code');
            if ($video->save())
            {
                Yii::$app->session->setFlash('success', 'Сведения о видео успешно изменены.');
                $this->redirect(['video']);
            }
        }
    }

    public function actionVideoDelete()
    {
        $request = Yii::$app->request;
        $video = Video::find()->where(['id' => (int)$request->get('id')])->one();
        if ($request->isGet)
        {
            return $this->render('videoDelete', ['video' => $video]);
        }
        if ($request->isPost)
        {
            if ($video->delete())
            {
                /*$categories = BusinessEnvironment::find()->all();
                foreach ($categories as $category)
                {
                    $scenario = Scenario::find()->where(['categoryID' => $category->id])->one();
                    if ((int)$scenario['settings']['videoID'] == (int)$request->get('id'))
                    {
                        $scenario['settings']['videoID'] = null;
                        $scenario['settings']['hasVideo'] = false;
                        $scenario->save();
                    }
                }*/
                Yii::$app->session->setFlash('success', 'Видео успешно удалено.');
                $this->redirect(['video']);
            }
        }
    }

    public function actionDocument()
    {
        return $this->render('documents');
    }

    public function actionUpdateDocument()
    {
        $request = Yii::$app->request;
        if(Yii::$app->request->isGet)
        {
            $id = (int)$request->get('id');
            $document = Documents::find()->where(['id' => $id])->one();
            return $this->render('updateDocumentForm', ['document' => $document]);
        }
        if(Yii::$app->request->isPost)
        {
            $id = (int)$request->get('id');
            $document = Documents::find()->where(['id' => $id])->one();
            $document->short_name = $request->post('ShortName');
            $document->full_name = $request->post('FullName');
            $document->save();
            Yii::$app->session->setFlash('success', 'Изменения успешно сохранены.');
            return $this->actionDocument();
        }
    }

    public function actionDocumentUpload()
    {
        $model = new UploadDocumentForm();

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file && $model->validate()) {
                $model->file->saveAs('../documents/' . FilesController::transliteration($model->file->baseName) . '.' . $model->file->extension);
                $document = new Documents();
                if (Documents::find()->count() == 0)
                {
                    $document->id = 1;
                }
                else
                {
                    $document->id = Documents::find()->all()[Documents::find()->count() - 1]->id + 1;
                }

                $document->url = './documents/' . FilesController::transliteration($model->file->baseName) . '.' . $model->file->extension;
                $document->full_name = Yii::$app->request->post('UploadDocumentForm')['fullDocName'];
                $document->short_name = Yii::$app->request->post('UploadDocumentForm')['shortDocName'];
                $document->extension = $model->file->extension;
                $document->save();
                Yii::$app->session->setFlash('success', 'Документ успешно загружен.');
            }
        }
        return $this->render('document', ['model' => $model]);
    }

    public function actionDeleteDocument()
    {
        $request = Yii::$app->request;
        if(Yii::$app->request->isGet)
        {
            $id = (int)$request->get('id');
            $document = Documents::find()->where(['id' => $id])->one();
            return $this->render('documentDelete', ['document' => $document]);
        }
        if(Yii::$app->request->isPost)
        {
            $id = (int)$request->get('id');
            $document = Documents::find()->where(['id' => $id])->one();
            $path = '.' . $document->url;
            $document->delete();
            unlink($path);
            Yii::$app->session->setFlash('success', 'Документ успешно удален.');
            return $this->actionDocument();
        }
    }

    public static function transliteration($text){

        $trans_arr = array (
            "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
            "е"=>"e", "ё"=>"yo","ж"=>"j","з"=>"z","и"=>"i",
            "й"=>"i","к"=>"k","л"=>"l", "м"=>"m","н"=>"n",
            "о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t",
            "у"=>"y","ф"=>"f","х"=>"h","ц"=>"c","ч"=>"ch",
            "ш"=>"sh","щ"=>"sh","ы"=>"i","э"=>"e","ю"=>"u",
            "я"=>"ya",
            "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
            "Е"=>"E","Ё"=>"Yo","Ж"=>"J","З"=>"Z","И"=>"I",
            "Й"=>"I","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
            "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
            "У"=>"Y","Ф"=>"F","Х"=>"H","Ц"=>"C","Ч"=>"Ch",
            "Ш"=>"Sh","Щ"=>"Sh","Ы"=>"I","Э"=>"E","Ю"=>"U",
            "Я"=>"Ya",
            "ь"=>"","Ь"=>"","ъ"=>"","Ъ"=>"",
            "ї"=>"j","і"=>"i","ґ"=>"g","є"=>"ye",
            "Ї"=>"J","І"=>"I","Ґ"=>"G","Є"=>"YE",
            " " => ""
        );

        return strtr($text, $trans_arr);

    }
}
