<?php

namespace backend\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use Yii;
use common\models\Settings;

class SettingsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionBase()
    {
        $request = Yii::$app->request;
        /*
         * Вывод формы базовых настроек
         */
        if(Yii::$app->request->isGet)
        {
            $siteName = Settings::getSiteName();
            $siteStatus = Settings::getSiteStatus();
            $settings = [
                'siteName' => $siteName,
                'siteStatus' => $siteStatus
            ];
            return $this->render('base', ['settings' => $settings]);
        }
        /*
         *Сохранение базовых настроек
         */
        if(Yii::$app->request->isPost)
        {
            $siteName = $request->post('siteName');
            Settings::setSiteName($siteName);

            $siteStatus = $request->post('siteStatus');
            Settings::setSiteStatus($siteStatus);

            Yii::$app->session->setFlash('success', 'Изменения успешно сохранены.');
            return $this->redirect(['base']);
        }

    }

    /**
     * Установка логотипа сайта
     */
    public function actionLogo()
    {
        $request = Yii::$app->request;
        $file = $request->get('file');
        $path = strtr($file, ['..' => '.']);
        Settings::setLogo($path);
        Yii::$app->session->setFlash('success', 'Изображение установлено как логотип.');
        return $this->redirect(['files/image']);
    }

}
