<?php

namespace backend\controllers;

use Yii;
use common\models\DataCollection;
use yii\helpers\Json;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class CollectionsController extends Controller
{
    public $layout = 'collectionsEditor';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id === 'create' || $action->id === 'save' || $action->id === 'delete') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * Функция рендеринга приложения работы с коллекциями
     */
    public function actionWorkspace()
    {
        return $this->render('workspace');
    }

    /**
     * Функция выгрузки коллекций из БД в клиентское приложение
     */
    public function actionLoad()
    {
        $collections = DataCollection::find()->all();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = $collections;
        $response->send();
    }

    /**
     * @return bool
     * Функция создания новой коллекции
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $newCollection = $request->post('collection');
        $newCollection = Json::decode($newCollection);
        $collection = new DataCollection();
        $collection->collection_name = $newCollection['collection_name'];
        $collection->settings = [];
        $collection->structure = $newCollection['structure'];
        $collection->content = [];
        return $collection->save();
    }

    public function actionSave()
    {
        $request = Yii::$app->request;
        $savingCollection = $request->post('collection');
        $savingCollection = Json::decode($savingCollection);
        $collection = DataCollection::find()->where(['collection_name' => $savingCollection['collection_name']])->one();
        $collection->settings = $savingCollection['settings'];
        $collection->structure = $savingCollection['structure'];
        $collection->content = $savingCollection['content'];
        return $collection->update();
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $collectionName = $request->post('collection_name');
        $collection = DataCollection::find()->where(['collection_name' => $collectionName])->one();
        return $collection->delete();
    }
}
