<?php

namespace backend\controllers;

use common\models\Settings;
use Yii;
use yii\filters\AccessControl;

class PagesController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionHello()
    {
        $request = Yii::$app->request;
        if ($request->isPost)
        {
            $page = $request->post('Page');
            Settings::setHelloPage($page);
            Yii::$app->session->setFlash('success', 'Изменения успешно сохранены.');
            return $this->redirect(['pages/hello']);
        }
        else {
            $page = Settings::getHelloPage();
            return $this->render('helloForm', ['page' => $page]);
        }
    }

    public function actionContacts()
    {
        $request = Yii::$app->request;
        if ($request->isPost)
        {
            $page = $request->post('Page');
            Settings::setContactsPage($page);
            Yii::$app->session->setFlash('success', 'Изменения успешно сохранены.');
            return $this->redirect(['pages/contacts']);
        }
        else {
            $page = Settings::getContactsPage();
            return $this->render('contactsForm', ['page' => $page]);
        }
    }

    public function actionHelp()
    {
        $request = Yii::$app->request;
        if ($request->isPost)
        {
            $page = $request->post('Page');
            Settings::setHelpPage($page);
            Yii::$app->session->setFlash('success', 'Изменения успешно сохранены.');
            return $this->redirect(['pages/help']);
        }
        else
        {
            $page = Settings::getHelpPage();
            return $this->render('helpForm', ['page' => $page]);
        }
    }

    public function actionPartnership()
    {
        $request = Yii::$app->request;
        if ($request->isPost)
        {
            $page = $request->post('Page');
            Settings::setPartnershipPage($page);
            Yii::$app->session->setFlash('success', 'Изменения успешно сохранены.');
            return $this->redirect(['pages/partnership']);
        }
        else
        {
            $page = Settings::getPartnershipPage();
            return $this->render('partnershipForm', ['page' => $page]);
        }
    }
}
