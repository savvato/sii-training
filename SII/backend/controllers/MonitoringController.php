<?php

namespace backend\controllers;

use Yii;
use common\models\Feedback;
use yii\web\Controller;
use yii\filters\AccessControl;

class MonitoringController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionFeedbacks()
    {
        $countOfNotReadedFeedbacks = Feedback::getCountOfUnreadMessages();
        return $this->render('feedbacks', ['countOfNotReadedFeedbacks' => $countOfNotReadedFeedbacks]);
    }

    public function actionShowFeedback()
    {
        $request = Yii::$app->request;
        $id = (int)$request->get('id');
        $feedback = Feedback::find()->where(['id' => $id])->one();
        $feedback->is_readed = true;
        $feedback->save();
        return $this->render('feedback', ['feedback' => $feedback]);
    }
}
