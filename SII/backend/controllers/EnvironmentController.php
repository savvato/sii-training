<?php

namespace backend\controllers;
use common\models\BusinessEnvironment;
use common\models\Scenario;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;


class EnvironmentController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        if(Yii::$app->request->isGet)
        {
            return $this->render('createForm');
        }
        if(Yii::$app->request->isPost)
        {
            $categoryName = $request->post('CategoryName');
            $categoryStatus = (int)$request->post('Main');

            //Проверка уникальности имени
            $exclusiveName = true;
            $categories = BusinessEnvironment::find()->all();
            foreach ($categories as $category)
            {
                if ($category->name == $categoryName)
                {
                    $exclusiveName = false;
                    break;
                }
            }
            if ($exclusiveName)
            {
                $category = new BusinessEnvironment();
                if (BusinessEnvironment::find()->count() == 0)
                {
                    $category->id = 1;
                }
                else
                {
                    $category->id = BusinessEnvironment::find()->all()[BusinessEnvironment::find()->count() - 1]->id + 1;
                }
                $category->name = $categoryName;
                $category->main = $categoryStatus;
                $category->save();
                Scenario::createEmptyScenario($category->id);
                Yii::$app->session->setFlash('success', 'Новая категория успешно создана.');
                return $this->redirect(['index']);
            }
            else
            {
                Yii::$app->session->setFlash('error', 'Категория с таким наименованием уже создана.');
                return $this->redirect(['index']);
            }
        }
    }

    public function actionUpdate()
    {
        $request = Yii::$app->request;
        if(Yii::$app->request->isGet)
        {
            $id = (int)$request->get('id');
            $category = BusinessEnvironment::find()->where(['id' => $id])->one();
            return $this->render('updateForm', ['category' => $category]);
        }
        if(Yii::$app->request->isPost)
        {
            $id = (int)$request->get('id');
            $category = BusinessEnvironment::find()->where(['id' => $id])->one();
            $categoryName = $request->post('CategoryName');
            $categoryStatus = $request->post('Main');
            $category->name = $categoryName;
            $category->main = $categoryStatus;
            $category->save();
            Yii::$app->session->setFlash('success', 'Изменения успешно сохранены.');
            return $this->actionindex();
        }

    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        if(Yii::$app->request->isGet)
        {
            $id = (int)$request->get('id');
            $category = BusinessEnvironment::findOne($id);
            return $this->render('deleteForm', ['category' => $category]);
        }
        if(Yii::$app->request->isPost)
        {
            $id = (int)$request->get('id');
            $category = BusinessEnvironment::find()->where(['id' => $id])->one();
            Scenario::deleteScenario($category->id);
            $category->delete();
            Yii::$app->session->setFlash('success', 'Категория успешно удалена.');
            return $this->actionindex();
        }
    }
}
