<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use backend\controllers\FilesController;

class UploadImageForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'gif, jpg, png', 'maxFiles' => 10],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => 'Выберите изображения (до 10):',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $file->saveAs('../images/' . FilesController::transliteration($file->baseName) . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }
}