<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadDocumentForm extends Model
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;
    public $fullDocName;
    public $shortDocName;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                ['file'],
                'file',
                'checkExtensionByMimeType' => false,
                'extensions' => 'doc, docx, pdf, xls, xlsx',
                'maxSize' => 21485760
            ],
            [['fullDocName', 'shortDocName'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Выберите документ:',
            'fullDocName' => 'Полное наименование документа:',
            'shortDocName' => 'Краткое наименование документа:',
        ];
    }
}