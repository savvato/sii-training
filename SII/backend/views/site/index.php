<?php
/* @var $this yii\web\View */
use common\models\Feedback;
use \common\models\BusinessEnvironment;
use \common\models\DataCollection;
use yii\helpers\Url;
$this->title = 'СИВ';
$countUnreadMessages = Feedback::getCountOfUnreadMessages();
$countEnv = BusinessEnvironment::find()->count();
$countCollections = DataCollection::find()->count();
?>
<div class="site-index">
    <div class="container-fluid">
        <h3 style="text-align: center">СИСТЕМА ИНФОРМАЦИОННОГО ВОЗДЕЙСТВИЯ</h3>
        <br>
        <div class="col-md-4">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $countEnv ?></h3>

                    <p>Категорий делового окружения</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <div class="small-box-footer">
                    <a href="<?= Url::to(['environment/index']) ?>" >
                        Список категорий <i class="fa fa-arrow-circle-right"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>

                    <a href="<?= Url::to(['scenario/index']) ?>">
                        Сценарии <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
                </div>

        </div>
        <div class="col-md-4">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $countCollections ?></h3>

                    <p>Коллекций данных</p>
                </div>
                <div class="icon">
                    <i class="fa fa-database"></i>
                </div>
                <a href="<?= Url::to(['collections/workspace']) ?>" class="small-box-footer">
                    Просмотреть <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?= $countUnreadMessages ?></h3>

                    <p>Новых сообщений</p>
                </div>
                <div class="icon">
                    <i class="fa fa-envelope-o"></i>
                </div>
                <a href="<?= Url::to(['monitoring/feedbacks']) ?>" class="small-box-footer">
                    Просмотреть <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <p>Версия системы: 1.0.2</p>
        <p>Дата сборки: 2016-03-20</p>
    </div>
</div>
