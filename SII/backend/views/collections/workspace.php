<?php

use yii\helpers\FileHelper;
use yii\helpers\Url;

$files = FileHelper::findFiles('../images');
$list = "[";
foreach ($files as $index => $file)
{
    $file = strtr($file, ['\\' => '/']);
    $list .= '{url: "' . $file . '"},';
}
$list .= "]";

$this->title = 'Коллекции данных';
?>

<!--ПЕРВИЧНЫЕ ДАННЫЕ, НЕОБХОДИМЫЕ ДЛЯ ПРИЛОЖЕНИЯ-->
<div style="display: none" ng-init='images = <?= $list ?>'></div>
<div style="display: none" ng-init="pathToLoad = '<?= Url::to(["collections/load"]) ?>'"></div>
<div style="display: none" ng-init="pathToCreate = '<?= Url::to(["collections/create"]) ?>'"></div>
<div style="display: none" ng-init="pathToSave = '<?= Url::to(["collections/save"]) ?>'"></div>
<div style="display: none" ng-init="pathToDelete = '<?= Url::to(["collections/delete"]) ?>'"></div>

<div class="container-fluid" style="padding: 10px 30px; height: auto;">
    <div ng-if="mode == 'creatingCollection'" ng-controller="CreatingCollectionController">
        <h3 style="margin-bottom: 30px">{{currentCollection.collection_name}}</h3>

        <form class="form">
            <div class="form-group">
                <label for="inputCollectionName" class="col-sm-1 control-label">Название коллекции:</label>
                <div class="col-sm-11">
                    <input type="text" ng-model="currentCollection.collection_name" class="form-control" id="inputCollectionName">
                </div>
            </div>
        </form>

        <br><br><br>
        <h4>Структура данных:</h4>
        <ul class="list-group" style="overflow: visible">
            <li class="list-group-item" ng-repeat="field in currentCollection.structure">
                <button class="btn btn-danger pull-right" ng-click="deleteField(this.$index)"><i class="fa fa-times"></i> Удалить атрибут</button>
                <h4>{{field.title}} <small>{{field.type.name}}</small></h4>

            </li>
            <li class="list-group-item list-group-item-success" style="padding: 15px;">
                <button class="btn btn-success btn-block" ng-show="!addingField" ng-click="showAddFieldForm()">
                    <i class="fa fa-plus"></i> Добавить поле
                </button>
                <form class="form" style="overflow: visible" ng-show="addingField">
                    <br>
                    <div class="form-group">
                        <label for="inputNewFieldTitle" class="col-sm-2 control-label">Название поля:</label>
                        <div class="col-sm-10">
                            <input type="text" ng-model="newField.title" class="form-control" id="inputNewFieldTitle">
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group">
                        <label for="inputNewFieldType" class="col-sm-2 control-label">Тип поля:</label>
                        <div class="col-sm-10">
                            <ui-select ng-model="newField.type" >
                                <ui-select-match>
                                    <span ng-bind="$select.selected.name"></span>
                                </ui-select-match>
                                <ui-select-choices repeat="type in (fieldTypes)">
                                    <i class="fa {{type.icon}}"></i> {{type.name}}
                                </ui-select-choices>
                            </ui-select>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group">
                        <label for="inputNewFieldRequired" class="col-sm-2 control-label">Обязательное поле:</label>
                        <div class="col-sm-10">
                            <input type="checkbox" ng-model="newField.required" class="checkbox" id="inputNewFieldRequired">
                        </div>
                    </div>
                    <br><br>
                    <div style="display: flex;">
                        <button style="width: 48%; margin: auto" class="btn btn-success" ng-click="addField()"><i class="fa fa-plus"></i> Добавить поле</button>
                        <button style="width: 48%; margin: auto" class="btn btn-danger" ng-click="closeAddFieldForm()"><i class="fa fa-times"></i> Отмена</button>
                    </div>

                </form>
            </li>
        </ul>
        <br>
        <button class="btn btn-success" ng-click="createCollection()"><i class="fa fa-floppy-o"></i> Создать коллекцию</button>
    </div>
    <div ng-if="mode == 'editingCollection'" ng-controller="EditingCollectionController">

        <div class="nav-tabs-custom" style="margin-bottom: 80px">
            <ul class="nav nav-tabs">
                <li class="header" style="margin-top: 0px;">
                    <i class="fa fa-database"> </i> {{currentCollection.collection_name}}
                </li>
                <li class="active"><a href="#meta" data-toggle="tab">Метаданные коллекции</a></li>
                <li><a href="#records" data-toggle="tab">Записи</a></li>
                <li><a href="#settings" data-toggle="tab">Настройки</a></li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="meta">
                    <button class="btn btn-danger pull-right" ng-click="deleteCollection()"><i class="fa fa-times"></i> Удалить коллекцию</button>
                    <h4>Всего записей: {{currentCollection.content.length}}</h4>

                    <hr>
                    <h4>Структура данных коллекции <small>Атрибуты записей</small></h4>
                    <ul class="list-group" style="overflow: visible">
                        <li class="list-group-item" ng-repeat="field in currentCollection.structure">
                            <h4>{{field.title}} <small>{{field.type.name}}</small></h4>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane" id="records">
                    <h4>Контент коллекции <small>Записи</small></h4>
                    <div class="panel-group" id="accordion" style="overflow: visible">
                        <div class="panel panel-default" ng-repeat="record in currentCollection.content">
                            <div class="panel-heading">
                                <button class="btn btn-danger btn-xs pull-right" style="position: relative; top: -2px;" ng-click="deleteRecord(this.$index)">
                                    <i class="fa fa-times"></i> Удалить запись
                                </button>
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{this.$index}}">
                                        Запись #{{this.$index + 1}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{this.$index}}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form class="form" style="overflow: visible">
                                        <div ng-repeat="field in currentCollection.structure">
                                            <div class="form-group" >
                                                <label class="col-sm-2 control-label">{{field.title}}:</label>
                                                <div ng-if="field.type.type_name != 'image'">
                                                    <div class="col-sm-10">
                                                        <input type="{{field.type.type_name}}" ng-model="record[field.name]" class="form-control">
                                                    </div>
                                                </div>
                                                <div ng-if="field.type.type_name == 'image'">
                                                    <div class="col-sm-10">
                                                        <ui-select ng-model="record[field.name]">
                                                            <ui-select-match>
                                                                <span ng-bind="$select.selected.url"></span>
                                                            </ui-select-match>
                                                            <ui-select-choices repeat="image in (images)">
                                                                <img src="{{image.url}}" style="height: 50px;">
                                                            </ui-select-choices>
                                                        </ui-select>
                                                    </div>
                                                </div>

                                            </div>
                                            <br><br>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="cursor: pointer" ng-click="addingNewRecord()">
                                        Добавить запись
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseAdd" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form class="form" style="overflow: visible" name="addForm">
                                        <div ng-repeat="field in currentCollection.structure">
                                            <div class="form-group" >
                                                <label for="input{{field.name}}" class="col-sm-2 control-label">{{field.title}}:
                                                    <small class="text-danger" ng-if="field.required"><br>Обязательное поле</small></label>
                                                <div ng-if="field.type.type_name != 'image'">
                                                    <div class="col-sm-10">
                                                        <input id="input{{field.name}}" type="{{field.type.type_name}}" ng-required="field.required" ng-model="newRecord[field.name]" class="form-control">
                                                    </div>
                                                </div>
                                                <div ng-if="field.type.type_name == 'image'">
                                                    <div class="col-sm-10" style="z-index: 50">
                                                        <ui-select ng-model="newRecord[field.name]">
                                                            <ui-select-match>
                                                                <span ng-bind="$select.selected.url"></span>
                                                            </ui-select-match>
                                                            <ui-select-choices repeat="image in (images)">
                                                                <img src="{{image.url}}" style="height: 50px;">
                                                            </ui-select-choices>
                                                        </ui-select>
                                                    </div>
                                                </div>

                                            </div>
                                            <br><br><br>
                                        </div>
                                        <br>
                                        <div style="display: flex;">
                                            <button style="width: 48%; margin: auto" class="btn btn-success" ng-click="addRecord(addForm)"><i class="fa fa-plus"></i> Добавить запись</button>
                                            <button style="width: 48%; margin: auto" class="btn btn-danger" ng-click="closeAddRecordForm()"><i class="fa fa-times"></i> Отмена</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success" ng-click="saveCollection()"><i class="fa fa-floppy-o"></i> Сохранить изменения</button>
                </div>
                <div class="tab-pane" id="settings"></div>
            </div>
        </div>



        </div>
</div>
