<?php

use yii\helpers\FileHelper;
use yii\helpers\Url;
use common\models\Documents;
use yii\helpers\Json;

$this->title = 'Редактор сценария информационного воздействия';

$files = FileHelper::findFiles('../images');
$list = "[";
foreach ($files as $index => $file)
{
    $file = strtr($file, ['\\' => '/']);
    $list .= '{url: "' . $file . '"},';
}
$list .= "]";
$this->params['categoryID'] = $id;

$documents = Documents::find()->all();
$documents = Json::encode($documents);
?>

<!--ПЕРВИЧНЫЕ ДАННЫЕ, НЕОБХОДИМЫЕ ДЛЯ ПРИЛОЖЕНИЯ-->
<div style="display: none" ng-init="categoryID = <?= $id ?>"></div>
<div style="display: none" ng-init='images = <?= $list ?>'></div>
<div style="display: none" ng-init="pathToSave = '<?= Url::to(["scenario/save"]) ?>'"></div>
<div style="display: none" ng-init="pathToLoad = '<?= Url::to(["scenario/load"]) ?>'"></div>
<div style="display: none" ng-init="pathToCollectionsLoad = '<?= Url::to(["collections/load"]) ?>'"></div>
<div style="display: none" ng-init='documents = <?= $documents ?>'></div>

<div ng-controller="WorkspaceController">

    <!--ТИП СТРАНИЦЫ - ОБЫЧНАЯ СТРАНИЦА-->
    <div ng-if="currentPage.type == 'simplePage'">
        <div ng-repeat="section in currentPage.content_structure" class="section">
            <div ng-include="getTemplate(section)" ></div>
            <div class="controls">
                <button ng-if="section.hasBlocks" class="btn btn-success btn-xs btn-circle" ng-click="addBlock(section)">
                    <i class="fa fa-plus"></i>
                </button>
                <button class="btn btn-default btn-xs btn-circle" ng-click="sectionUp(this.$index)" ng-if="this.$index != 0">
                    <i class="fa fa-arrow-up"></i>
                </button>
                <button class="btn btn-default btn-xs btn-circle" ng-click="sectionDown(this.$index)" ng-if="this.$index != currentPage.content_structure.length - 1">
                    <i class="fa fa-arrow-down"></i>
                </button>
                <button class="btn btn-default btn-xs btn-circle" ng-click="getBlockSettings(section)">
                    <i class="fa fa-cog"></i>
                </button>
                <button class="btn btn-danger btn-xs btn-circle" ng-click="removeSection(this.$index)">
                    <i class="fa fa-times"></i>
                </button>
            </div>
        </div>
    </div>

    <!--ТИП СТРАНИЦЫ - СТРАНИЦА-СЛАЙДЕР-->
    <div ng-if="currentPage.type == 'sliderPage'">
        <div class="header section">
            <div class="cards container-fluid">
                <div class="col-md-3 has-blocks"
                     href="#main-carousel"
                     data-slide-to="{{this.$index}}"
                     ng-repeat="card in currentPage.content_structure"
                    >
                    <div ng-include="getTemplate(card.block)"></div>

                    <div class="controls">
                        <button class="btn btn-default btn-xs btn-circle" ng-click="slideLeft(this.$index)" ng-if="this.$index != 0">
                            <i class="fa fa-arrow-left"></i>
                        </button>
                        <button class="btn btn-default btn-xs btn-circle" ng-click="slideRight(this.$index)" ng-if="this.$index != currentPage.content_structure.length - 1">
                            <i class="fa fa-arrow-right"></i>
                        </button>
                        <button class="btn btn-default btn-xs btn-circle" ng-click="getBlockSettings(card.block)">
                            <i class="fa fa-cog"></i>
                        </button>
                        <button class="btn btn-danger btn-xs btn-circle" ng-click="removeSlide(this.$index)">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="controls">
                <button class="btn btn-success btn-xs btn-circle" ng-click="addSlide()">
                    <i class="fa fa-plus"></i>
                </button>
                <button class="btn btn-default btn-xs btn-circle" ng-click="getSliderPageSettings()">
                    <i class="fa fa-cog"></i>
                </button>
            </div>
        </div>

        <div id="main-carousel" class="carousel slide">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item {{(this.$index == 0) ? 'active' : ''}}" ng-repeat="slide in currentPage.content_structure">
                    <div ng-repeat="section in slide.content_structure" class="section">
                        <div ng-include="getTemplate(section)" ></div>
                        <div class="controls">
                            <button ng-if="section.hasBlocks" class="btn btn-success btn-xs btn-circle" ng-click="addBlock(section)">
                                <i class="fa fa-plus"></i>
                            </button>
                            <button class="btn btn-default btn-xs btn-circle" ng-click="sectionUp(this.$index)" ng-if="this.$index != 0">
                                <i class="fa fa-arrow-up"></i>
                            </button>
                            <button class="btn btn-default btn-xs btn-circle" ng-click="sectionDown(this.$index)" ng-if="this.$index != getWorkspace().content_structure.length - 1">
                                <i class="fa fa-arrow-down"></i>
                            </button>
                            <button class="btn btn-default btn-xs btn-circle" ng-click="getBlockSettings(section)">
                                <i class="fa fa-cog"></i>
                            </button>
                            <button class="btn btn-danger btn-xs btn-circle" ng-click="removeSection(this.$index)">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--ТИП СТРАНИЦЫ - ФОРМА ОБРАТНОЙ СВЯЗИ-->
    <div ng-if="currentPage.type == 'feedbackPage'">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <h1>{{currentPage.step_name}}</h1>

                    <div medium-editor
                       bind-options="mediumBindOptions"
                       ng-model="currentPage.content_structure.introduction"
                       contenteditable="true">
                    </div>
                    <form id="contact-form" method="post" role="form">
                        <input type="hidden" name="ContactForm[category]" value="1">

                        <div class="form-group field-contactform-name required">
                            <label class="control-label" for="contactform-name">Ф.И.О.</label>
                            <input type="text" id="contactform-name" class="form-control" name="ContactForm[name]">
                            <p class="help-block help-block-error"></p>
                        </div>

                        <div class="form-group field-contactform-email required">
                            <label class="control-label" for="contactform-email">E-mail </label>
                            <input type="text" id="contactform-email" class="form-control" name="ContactForm[email]">
                            <p class="help-block help-block-error"></p>
                        </div>

                        <div class="form-group field-contactform-subject required">
                            <label class="control-label" for="contactform-subject">Тема</label>
                            <input type="text" id="contactform-subject" class="form-control" name="ContactForm[subject]">
                            <p class="help-block help-block-error"></p>
                        </div>

                        <div class="form-group field-contactform-body required">
                            <label class="control-label" for="contactform-body">Сообщение</label>
                            <textarea id="contactform-body" class="form-control" name="ContactForm[body]" rows="6"></textarea>
                            <p class="help-block help-block-error"></p>
                        </div>

                        <div class="form-group field-contactform-verifycode">
                            <label class="control-label" for="contactform-verifycode">Код проверки</label>
                            <div class="row">
                                <div class="col-lg-3">
                                    <img id="contactform-verifycode-image" src="/sii/www/index.php?r=site%2Fcaptcha&amp;v=56aa67bb0e31e" alt="">
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" id="contactform-verifycode" class="form-control" name="ContactForm[verifyCode]">
                                </div>
                            </div>
                            <p class="help-block help-block-error"></p>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" name="contact-button">Отправить</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>

    <!--ТИП СТРАНИЦЫ - ДОКУМЕНТЫ-->
    <div ng-if="currentPage.type == 'documentPage'">
        <div class="header section">
            <div class="cards container-fluid">
                <div class="col-md-3 has-blocks"
                     href="#main-carousel"
                     data-slide-to="{{this.$index}}"
                     ng-repeat="card in currentPage.content_structure"
                >
                    <div ng-include="getTemplate(card.block)"></div>

                    <div class="controls">
                        <button class="btn btn-default btn-xs btn-circle" ng-click="slideLeft(this.$index)" ng-if="this.$index != 0">
                            <i class="fa fa-arrow-left"></i>
                        </button>
                        <button class="btn btn-default btn-xs btn-circle" ng-click="slideRight(this.$index)" ng-if="this.$index != currentPage.content_structure.length - 1">
                            <i class="fa fa-arrow-right"></i>
                        </button>
                        <button class="btn btn-default btn-xs btn-circle" ng-click="getBlockSettings(card.block)">
                            <i class="fa fa-cog"></i>
                        </button>
                        <button class="btn btn-danger btn-xs btn-circle" ng-click="removeSlide(this.$index)">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="controls">
                <button class="btn btn-success btn-xs btn-circle" ng-click="addSlide()">
                    <i class="fa fa-plus"></i>
                </button>
                <button class="btn btn-default btn-xs btn-circle" ng-click="getSliderPageSettings()">
                    <i class="fa fa-cog"></i>
                </button>
            </div>
        </div>
        <div id="main-carousel" class="carousel slide" >
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item {{(this.$index == 0) ? 'active' : ''}}" ng-repeat="slide in currentPage.content_structure">
                    <!--ДОКУМЕНТЫ-->
                    <div ng-repeat="section in slide.content_structure" class="section">
                        <div ng-include="getTemplate(section)"></div>
                        <div class="controls">
                            <button class="btn btn-default btn-xs btn-circle" ng-click="sectionUp(this.$index)" ng-if="this.$index != 0">
                                <i class="fa fa-arrow-up"></i>
                            </button>
                            <button class="btn btn-default btn-xs btn-circle" ng-click="sectionDown(this.$index)" ng-if="this.$index != getWorkspace().content_structure.length - 1">
                                <i class="fa fa-arrow-down"></i>
                            </button>
                            <button class="btn btn-default btn-xs btn-circle" ng-click="getBlockSettings(section)">
                                <i class="fa fa-cog"></i>
                            </button>
                            <button class="btn btn-danger btn-xs btn-circle" ng-click="removeSection(this.$index)">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--МОДАЛЬНОЕ ОКНО ДЛЯ НАСТРОЕК СЕКЦИЙ И БЛОКОВ-->
    <div class="modal fade" id="block-settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Настройки блока</h4>
                </div>
                <div class="modal-body">
                    <div ng-include="getSettingsForm()"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="sliderPage-settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Настройки страницы</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="inputCardType" class="col-sm-3 control-label">Тип карточек:</label>
                            <div class="col-sm-9">
                                <select ng-model="cardType" id="inputCardType" class="form-control" ng-change="changeCardType(cardType)">
                                    <option value="definitiveCard">Простая карточка</option>
                                    <option value="definitivePhotoCard">Карточка с изображением</option>
                                    <option value="definitivePhoto">Фотокарточка</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
</div>