<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
$this->title = 'Сценарии информационного воздействия';
?>

<div class="container-fluid">
    <h1>Сценарии информационного воздействия</h1>
    <p>
        Выберите сценарий из списка для редактирования:
    </p>
    <?php foreach ($categories as $category): ?>
        <?= Html::a(
            $category["name"],
            ['scenario/editor', 'id' => $category["id"]],
            ['class' => 'btn btn-default', 'style' => 'width: 100%; text-align: left; font-size: 22px;']
        ) ?>
        <br>
        <br>
    <?php endforeach; ?>
</div>
