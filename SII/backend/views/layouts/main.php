<?php
use yii\BaseYii;
use backend\assets\AppAsset;
use yii\helpers\Html;
use common\models\Feedback;
use frontend\widgets\Alert;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="padding: 0">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">ПОДГОТОВКА</li>
                <li <?php if (Yii::$app->request->getUrl() == Url::to(['settings/base'])) echo 'class="active"' ?>>
                    <a href="<?= Url::to(['settings/base']) ?>">Базовые настройки</a>
                </li>
                <li>
                    <a href="#"> Работа с файлами</a>
                    <ul class="treeview-menu">
                        <li <?php if (Yii::$app->request->getUrl() == Url::to(['files/image'])) echo 'class="active"' ?>><a href="<?= Url::to(['files/image']) ?>"> Изображения</a></li>
                        <li <?php if (Yii::$app->request->getUrl() == Url::to(['files/video'])) echo 'class="active"' ?>><a href="<?= Url::to(['files/video']) ?>"> Видео</a></li>
                        <li <?php if (Yii::$app->request->getUrl() == Url::to(['files/document'])) echo 'class="active"' ?>><a href="<?= Url::to(['files/document']) ?>"> Документы</a></li>
                    </ul>
                </li>

                <li <?php if (Yii::$app->request->getUrl() == Url::to(['environment/index'])) echo 'class="active"' ?>>
                    <a href="<?= Url::to(['environment/index']) ?>">Деловое окружение</a>
                </li>
                <li class="header">УПРАВЛЕНИЕ КОНТЕНТОМ</li>
                <li class="treeview">
                    <a href="#">
                        <span>Стандартные страницы</span>
                    </a>
                    <ul class="treeview-menu">
                        <li <?php if (Yii::$app->request->getUrl() == Url::to(['pages/hello'])) echo 'class="active"' ?>>
                            <a href="<?= Url::to(['pages/hello']) ?>">Приветствие</a>
                        </li>
                        <li <?php if (Yii::$app->request->getUrl() == Url::to(['pages/contacts'])) echo 'class="active"' ?>>
                            <a href="<?= Url::to(['pages/contacts']) ?>">Контакты</a>
                        </li>
                        <li <?php if (Yii::$app->request->getUrl() == Url::to(['pages/help'])) echo 'class="active"' ?>>
                            <a href="<?= Url::to(['pages/help']) ?>">Помощь</a>
                        </li>
                        <li <?php if (Yii::$app->request->getUrl() == Url::to(['pages/partnership'])) echo 'class="active"' ?>>
                            <a href="<?= Url::to(['pages/partnership']) ?>">Партнерство</a>
                        </li>
                    </ul>
                </li>
                <li <?php if (Yii::$app->request->getUrl() == Url::to(['scenario/index'])) echo 'class="active"' ?>>
                    <a href="<?= Url::to(['scenario/index']) ?>"><span>Сценарии</span></a>
                </li>
                <li <?php if (Yii::$app->request->getUrl() == Url::to(['collections/workspace'])) echo 'class="active"' ?>>
                    <a href="<?= Url::to(['collections/workspace']) ?>"><span>Коллекции данных</span></a>
                </li>

                <li class="header">МОНИТОРИНГ</li>
                <li <?php if (Yii::$app->request->getUrl() == Url::to(['monitoring/feedbacks'])) echo 'class="active"' ?>>
                    <a href="<?= Url::to(['monitoring/feedbacks']) ?>">
                        <span>Обратная связь</span>
                        <?php
                        $countUnreadMessages = Feedback::getCountOfUnreadMessages();
                        if ($countUnreadMessages > 0): ?>
                        <small class="label pull-right bg-green"><?= $countUnreadMessages ?></small>
                        <?php endif; ?>
                    </a>
                </li>
                <li><a target="_blank" href="<?= Url::to('./analytics') ?>"><span>Аналитика</span></a></li>

                <li class="header">УПРАВЛЕНИЕ ПОЛЬЗОВАТЕЛЯМИ</li>
                <li>
                    <a>Роли</a>
                </li>
                <li>
                    <a>Пользователи</a>
                </li>

                <li class="header"><?= Yii::$app->user->identity->username ?></li>

                <li <?php if (Yii::$app->request->getUrl() == Url::to(['site/index'])) echo 'class="active"' ?>>
                    <a href="<?= Url::to(['site/index']) ?>"><span>Главное меню</span></a>
                </li>

                <li>
                    <a href="../" target="_blank"><span>Web-ресурс</span></a>
                </li>

                <li> <a data-method="post" href="<?= Url::to(['site/logout']) ?>"><span>Выход</span></a></li>


        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="height: 100%;">
        <div class="alert-container"><?= Alert::widget() ?></div>
        <?= $content ?>
    </div><!-- /.content-wrapper -->
</div><!-- ./wrapper -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
