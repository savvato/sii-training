<?php
use backend\assets\CollectionsAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

CollectionsAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="DataCollectionsApp">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>
<body class="hold-transition skin-blue" ng-controller="MainController">
<div class="wrapper">

    <header class="main-header">
        <a class="logo" style="width: 60px;">
            <i class="fa fa-hdd-o fa-2x" style="margin-left: -4px; margin-top: 5px;"></i>
        </a>
        <a class="navbar-brand">Коллекции данных</a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-header">
                <div>
                    <a href="<?= Url::home() ?>" class="btn btn-default navbar-btn"><i class="fa fa-reply"></i> Назад в главное меню</a>
                </div>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="margin-top: 50px;">
            <ul class="sidebar-menu">
                <li ng-repeat="collection in collections" class="{{collection == currentCollection? 'active' : ''}}">
                    <a ng-click="editingCollection(collection)" class="sidebar-button">
                        <i class="fa fa-database fa-2x">  </i>
                        <span style="font-size: 20px; margin-left: 5px; white-space: normal;"> {{collection.collection_name}}</span>
                    </a>
                </li>
            </ul>
        </section>
        <button class="btn btn-success" style="width: 95%; margin-left: 5px; margin-right: 5px;" ng-click="creatingCollection()"><i class="fa fa-plus"></i> Создать коллекцию   </button>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Рабочая область -->
    <div class="content-wrapper" style="padding-bottom: 10px; height: auto;">
        <?= $content ?>
    </div>

</div>
</body>
<?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
