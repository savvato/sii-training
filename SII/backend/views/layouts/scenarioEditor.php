<?php
use backend\assets\ScenarioAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Video;

/* @var $this \yii\web\View */
/* @var $content string */

ScenarioAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="ScenarioApp">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>
<body class="hold-transition skin-blue sidebar-collapse sidebar-mini" >
<div class="wrapper" ng-controller="MainController">

    <header class="main-header">
        <a class="logo">
            <i class="fa fa-cube" style="font-size: 26px; margin-left: -4px"></i>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-header">
                <div>
                    <a class="navbar-brand">{{categoryName}}</a>
                    <a href="<?= Url::home() ?>" class="btn btn-default navbar-btn"><i class="fa fa-reply"></i> Назад в главное меню</a>
                    <button ng-click="save()" class="btn btn-default navbar-btn"><i class="fa fa-floppy-o"></i> Сохранить изменения</button>
                </div>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="margin-top: 50px;">
            <ul class="sidebar-menu">
                <li data-toggle="tooltip" data-placement="right" title="Управление сценарием">
                    <!--ССЫЛКА НА МОДАЛЬНОЕ ОКНО УПРАВЛЕНИЯ СЦЕНАРИЕМ-->
                    <a class="sidebar-button" data-toggle="modal" data-target="#scenario-modal">
                        <i class="fa fa-list" style="font-size: 22px; margin-left: -5px"></i>
                    </a>

                </li>

                <!--ССЫЛКА НА МОДАЛЬНОЕ ОКНО ВЫБОРА СТРАНИЦЫ ДЛЯ РЕДАКТИРОВАНИЯ-->
                <li data-toggle="tooltip" data-placement="right" title="Выбор страницы">
                    <a class="sidebar-button" data-toggle="modal" data-target="#select-modal">
                        <i class="fa fa-pencil-square-o" style="font-size: 22px; margin-left: -5px"></i>
                    </a>
                </li>

                <!--ССЫЛКА НА МОДАЛЬНОЕ ОКНО УПРАВЛЕНИЯ НАСТРОЙКАМИ СЦЕНАРИЯ-->
                <li data-toggle="tooltip" data-placement="right" title="Настройки сценария">
                    <a class="sidebar-button" data-toggle="modal" data-target="#settings-modal">
                        <i class="fa fa-cog" style="font-size: 22px; margin-left: -5px"></i>

                    </a>
                </li>

                <!--ССЫЛКА НА МОДАЛЬНОЕ ОКНО ДОБАВЛЕНИЯ РАЗДЕЛОВ В СТРУКТУРУ КОНТЕНТА-->
                <li data-toggle="tooltip" data-placement="right" title="Добавление секций на страницу">
                    <a ng-if="currentPage.type != 'feedbackPage'" class="sidebar-button" data-toggle="modal" data-target="#add-block-modal">
                        <i class="fa fa-plus-square" style="font-size: 22px; margin-left: -5px"></i>

                    </a>
                </li>

                <!--ССЫЛКА НА МОДАЛЬНОЕ ОКНО ПРОСМОТРА ДИНАМИЧЕСКИХ КОЛЛЕКЦИЙ ДАННЫХ-->
                <li data-toggle="tooltip" data-placement="right" title="Коллекции данных">
                    <a class="sidebar-button" data-toggle="modal" data-target="#collections-modal">
                        <i class="fa fa-database" style="font-size: 22px; margin-left: -5px"></i>
                    </a>
                </li>

                <li data-toggle="tooltip" data-placement="right" title="Справка">
                    <a class="sidebar-button" data-toggle="modal" data-target="#help-modal">
                        <i class="fa fa-question" style="font-size: 22px; margin-left: -5px"></i>
                    </a>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Рабочая область -->
    <div class="content-wrapper" style="padding-bottom: 10px;">
        <section class="content-header">
            <h1>
                Рабочая область
                <small>Предварительный вид страницы <b>{{currentPage.step_name}}</b> </small>
            </h1>
        </section>
        <div class="workspace"> <!---->
            <?= $content ?>
        </div>
    <div class="control-sidebar-bg"></div>
</div>

    <!--МОДАЛЬНЫЕ ОКНА-->
    <!--Модальное окно сценария ИВ-->
    <div class="modal fade" id="scenario-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-controller="ScenarioController">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Сценарий информационного воздействия</h4>
                </div>
                <div class="modal-body">
                    <button ng-click="addGroup()" class="btn btn-default"><i class="fa fa-plus"></i> Добавить группу</button>
                    <button ng-if="currentGroup != null || currentGroup != undefined" ng-click="addStep()" class="btn btn-default"><i class="fa fa-plus"></i> Добавить шаг</button>
                    <br>
                    <br>
                    <div ng-include="getForm()"></div>
                    <hr>

                    <div ui-tree="treeOptions">
                        <ol ui-tree-nodes="" ng-model="scenario">
                            <li ng-repeat="group in scenario" ui-tree-node >
                                <div ui-tree-handle style="margin-right: 5px; margin-left: 5px; {{this.$modelValue == currentGroup ? 'background: #CBE9CB' : null}}">
                                    {{group.group_name}}
                                    <a
                                        class="pull-right btn btn-danger"
                                        style="margin-right: 5px"
                                        data-nodrag
                                        title="Удалить группу"
                                        ng-click="removeGroup(this)">
                                        <span class="fa fa-times"></span>
                                    </a>
                                    <a
                                        class="pull-right btn btn-default"
                                        style="margin-right: 5px"
                                        data-nodrag
                                        title="Настройки группы"
                                        ng-click="editGroup(this)">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>

                                </div>
                                <ol ui-tree-nodes="" ng-model="group.steps">
                                    <li ng-repeat="step in group.steps" ui-tree-node>
                                        <div ui-tree-handle style="margin-right: 5px; margin-left: 5px; {{this.$modelValue == currentStep ? 'background: #CBE9CB' : null}}">
                                            {{step.step_name}}
                                            <a
                                                class="pull-right btn btn-danger"
                                                style="margin-right: 5px"
                                                data-nodrag
                                                title="Удалить шаг"
                                                ng-click="removeStep(this)">
                                                <span class="fa fa-times"></span>
                                            </a>
                                            <a
                                                class="pull-right btn btn-default"
                                                style="margin-right: 5px"
                                                data-nodrag
                                                title="Настройки шага"
                                                ng-click="editStep(this)">
                                                <span class="glyphicon glyphicon-pencil"></span>
                                            </a>

                                        </div>
                                    </li>
                                </ol>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <!--Модальное окно основных настроек сценария-->
    <div class="modal fade" id="settings-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-controller="SettingsController">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Настройки сценария</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="inputStatus" class="col-sm-3 control-label">Статус сценария</label>
                            <div class="col-sm-9">
                                <select ng-model="settings.status" id="inputStatus" class="form-control">
                                    <option value="Available">Доступен (опубликован)</option>
                                    <option value="NotAvailable">Недоступен (не опубликован)</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputHasVideo" class="col-sm-3 control-label">Имеет видео?</label>
                            <input id="inputHasVideo" type="checkbox" ng-model="settings.hasVideo" ng-change="onHasVideoChanged()">
                        </div>
                        <div class="form-group" ng-show="settings.hasVideo">
                            <label for="inputVideo" class="col-sm-3 control-label">Выберите видео</label>
                            <div class="col-sm-9">
                                <select ng-model="settings.videoID" id="inputVideo" class="form-control">
                                    <?php
                                    $videos = Video::find()->all();
                                    foreach ($videos as $video):
                                    ?>
                                        <option value="<?= $video->id ?>"><?= $video->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputHasSidebarBackground" class="col-sm-3 control-label">Навигационный раздел имеет картинку?</label>
                            <input id="inputHasSidebarBackground" type="checkbox" ng-model="settings.hasSidebarBackground" ng-change="onHasSidebarBackgroundChanged()">
                        </div>
                        <div class="form-group" ng-show="settings.hasSidebarBackground">
                            <label for="inputSidebarBackground" class="col-sm-3 control-label">Фон:</label>
                            <div class="col-sm-9">
                                <ui-select ng-model="settings.sidebarBackground">
                                    <ui-select-match>
                                        <span ng-bind="$select.selected.url"></span>
                                    </ui-select-match>
                                    <ui-select-choices repeat="image in (images)">
                                        <img src="{{image.url}}" style="height: 50px;">
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <!--Модальное окно выбора страницы для редактирования-->
    <div class="modal fade" id="select-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Выберите шаг для редактирования контента</h4>
                </div>
                <div class="modal-body">
                    <div ui-tree data-drag-enabled="false" data-nodrop-enabled="true">
                        <ol ui-tree-nodes="" ng-model="scenario">
                            <li ng-repeat="group in scenario" ui-tree-node>
                                <div ui-tree-handle style="margin-right: 5px; margin-left: 5px; {{this.$modelValue == currentGroup ? 'background: #CBE9CB' : null}}">
                                    {{group.group_name}}
                                </div>
                                <ol ui-tree-nodes="" ng-model="group.steps">
                                    <li ng-repeat="step in group.steps" ui-tree-node>
                                        <div ui-tree-handle style="margin-right: 5px; margin-left: 5px; {{this.$modelValue == currentStep ? 'background: #CBE9CB' : null}}">
                                            {{step.step_name}}
                                            <a
                                                class="pull-right btn btn-default"
                                                style="margin-right: 5px"
                                                data-nodrag
                                                title="Выбрать шаг"
                                                ng-click="setCurrentPage(this)">
                                                <span class="fa fa-magic"></span>
                                            </a>

                                        </div>
                                    </li>
                                </ol>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <!--Модальное окно выбора добавляемого раздела-->
    <div class="modal fade" id="add-block-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-controller="WorkspaceController">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Выберите раздел для добавления:</h4>
                </div>
                <div class="modal-body">
                    <!--Набор доступных блоков для обычной и слайдерной страницы-->
                    <div class="container-fluid" ng-if="currentPage.type != 'documentPage'">
                        <div class="row-fluid">
                            <div class="col-md-6">
                                <!--SEPARATOR-->
                                <div class="panel panel-default clicked" ng-click="addSection('separator')">
                                    <div class="panel-heading">Разделитель</div>
                                    <div class="panel-body">
                                        <img src="./js/ScenarioApp/previews/separator.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-default clicked" ng-click="addSection('tricks')">
                                    <div class="panel-heading">Фотокарточка с поясняющим текстом</div>
                                    <div class="panel-body">
                                        <img src="./js/ScenarioApp/previews/tricks.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-default clicked" ng-click="addSection('jumbotronSimple')">
                                    <div class="panel-heading">Jumbotron (один блок текста)</div>
                                    <div class="panel-body">
                                        <img src="./js/ScenarioApp/previews/jumbotronSimple.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-default clicked" ng-click="addSection('jumbotronMultiple')">
                                    <div class="panel-heading">Jumbotron (несколько внутренних блоков)</div>
                                    <div class="panel-body">
                                        <img src="./js/ScenarioApp/previews/jumbotronMultiple.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-default clicked" ng-click="addSection('contentCards')">
                                    <div class="panel-heading">Карточки</div>
                                    <div class="panel-body">
                                        <img src="./js/ScenarioApp/previews/contentCards.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Набор доступных секций для документной страницы-->
                    <div class="container-fluid" ng-if="currentPage.type == 'documentPage'">
                        <div class="row-fluid">
                            <div class="col-md-6">
                                <div class="panel panel-default clicked" ng-click="addSection('separator')">
                                    <div class="panel-heading">Разделитель</div>
                                    <div class="panel-body">
                                        <img src="./js/ScenarioApp/previews/separator.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-default clicked" ng-click="addSection('documentsSection')">
                                    <div class="panel-heading">Комплект документов</div>
                                    <div class="panel-body">
                                        <img src="./js/ScenarioApp/previews/docsSection.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <!--Модальное окно коллекций данных-->
    <div class="modal fade" id="collections-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Коллекции данных</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-md-4">
                            <ul class="nav nav-pills nav-stacked">
                                <li ng-repeat="collection in collections" class="{{collection == currentCollection? 'active' : ''}}" style="cursor: pointer">
                                    <a ng-click="setCurrentCollection(collection)">
                                        <i class="fa fa-database fa-2x">  </i>
                                        <span style="font-size: 20px; margin-left: 5px; white-space: normal;"> {{collection.collection_name}}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-8" ng-show="currentCollection" style="border-left: 1px solid #EEEEEE;">
                            <h3>{{currentCollection.collection_name}}</h3>
                            <hr>
                            <h4>Структура данных коллекции <small>Атрибуты записей</small></h4>
                            <ul class="list-group" style="overflow: visible">
                                <li class="list-group-item" ng-repeat="field in currentCollection.structure">
                                    <h4>{{field.title}} <small>{{field.type.name}}</small></h4>
                                </li>
                            </ul>
                            <hr>
                            <div class="tab-pane" id="records">
                                <h4>Контент коллекции <small>Записи</small></h4>
                                <div class="panel-group" id="accordion" style="overflow: visible">
                                    <div class="panel panel-default" ng-repeat="record in currentCollection.content">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{this.$index}}">
                                                    Запись #{{this.$index + 1}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse{{this.$index}}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form class="form" style="overflow: visible">
                                                    <div ng-repeat="field in currentCollection.structure">
                                                        <div class="form-group" >
                                                            <label class="col-sm-3 control-label">{{field.title}}:</label>
                                                            <div ng-if="field.type.type_name != 'image'">
                                                                <div class="col-sm-9">
                                                                    <input disabled type="{{field.type.type_name}}" ng-model="record[field.name]" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div ng-if="field.type.type_name == 'image'">
                                                                <input disabled type="text" ng-model="record[field.name].url" class="form-control">
                                                                <br>
                                                                <img src="{{record[field.name].url}}" style="height: 50px">
                                                            </div>

                                                        </div>
                                                        <br><br>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <!--Модальное окно справки-->
    <div class="modal fade" id="help-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Справка по работе с конструктором сценария</h4>
                </div>
                <div class="modal-body">
                    <h4>1. Основы</h4>
                    <h4>2. Управление сценарием информационного воздействия</h4>
                    <h4>3. Работа с контентом</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
