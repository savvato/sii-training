<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="col-md-2 sidebar list-group" >
                    <?php

                    $menuItems = [
                        ['label' => 'Главная', 'url' => ['/site/index']],
                        "<hr>",
                        "<h5>Настройки </h5>",
                        [
                            'label' => 'Базовые настройки',
                            'url' => ['/settings/base']
                        ],
                        [
                            'label' => 'Работа с файлами',
                            'items' => [
                                [
                                    'label' => 'Изображения',
                                    'url' => ['/files/image'],
                                ],
                                [
                                    'label' => 'Видео',
                                    'url' => ['/files/video']
                                ],
                                [
                                    'label' => 'Документы',
                                    'url' => ['/files/document']
                                ],
                            ],
                        ],
                        [
                            'label' => 'Деловое окружение',
                            'url' => ['/environment/index']
                        ],

                        "<hr>",

                        "<h5>Управление контентом </h5>",
                        [
                            'dropDownOptions' => ['class' => 'dropdown-menu-right'],
                            'label' => 'Стандартные страницы',
                            'items' => [
                                [
                                    'label' => 'Приветствие',
                                    'url' => ['/pages/hello'],
                                    'dropDownOptions' => ['class' => 'dropdown-submenu'],
                                ],
                                [
                                    'label' => 'Контакты',
                                    'url' => ['/pages/contacts']
                                ],
                                [
                                    'label' => 'Помощь',
                                    'url' => ['/pages/help']
                                ],
                                [
                                    'label' => 'Документы',
                                    'url' => ['/pages/documents']
                                ],
                                [
                                    'label' => 'Партнерство',
                                    'url' => ['/pages/partnership']
                                ],
                            ],

                        ],
                        [
                            'label' => 'Сценарии',
                            'url' => ['/scenario/index']
                        ],

                        "<hr>",

                        "<h5>Управление пользователями </h5>",

                        "<hr>",

                        "<h5>Мониторинг информационного воздействия </h5>",
                        [
                            'label' => 'Обратная связь',
                            'url' => ['/monitoring/feedbacks']
                        ],
                        [
                            'label' => 'Мониторинг поведения',
                            'url' => './analytics'
                        ],

                        "<hr>"

                    ];
                    if (Yii::$app->user->isGuest) {
                        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
                    } else {
                        $menuItems[] = [
                            'label' => 'Выход (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']
                        ];
                    }
                    echo Nav::widget([
                        'options' => ['class' => 'nav-pills nav-stacked list-group-item'], //nav-list bs-docs-sidenav nav-collapse nav-pills nav-stacked list-group-item
                        'items' => $menuItems,

                    ]);

                    ?>
                </div>
                <div class="col-md-10 main">
                    <?= Alert::widget() ?>
                    <?= $content ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
