<?php
use yii\helpers\Html;
$this->title = 'Изменить категорию делового окружения';
?>
<div class="container-fluid">
    <h1><?= $category->name ?></h1>
    <?php
    $form = Html::beginForm(['environment/update', 'id' => $category->id], 'post');
    $form .= "<br>";
    $form .= "<div class='form-group'>";
    $form .= Html::label('Название категории:', ['class' => 'control-label']);
    $form .= Html::input('text', 'CategoryName', $category->name, ['class' => 'form-control']);
    $form .= "</div class='form-group'>";
    $form .= "<div class='form-group'>";
    $form .= Html::label('Статус категории:', ['class' => 'control-label']);
    $form .= Html::dropDownList('Main', $category->main, [1 => 'Основная', 0 => 'Дополнительная'], ['class' => 'form-control']);
    $form .= "</div class='form-group'>";
    $form .= Html::submitButton('<i class="fa fa-floppy-o"></i> Сохранить изменения', ['class' => 'btn btn-success']);
    $form .= Html::endForm();
    echo $form;
    ?>
</div>