<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Удалить категорию';
?>
<div class="container-fluid">
    <h1><?= $category->name ?></h1>
    <p class="text-danger">Вы действительно хотите удалить данную категорию делового окружения?</p>
    <div class="row-fluid">

        <?php
        $form = Html::beginForm(['environment/delete', 'id' => $category->id], 'post');
        $form .= Html::submitButton('<span class="glyphicon glyphicon-trash"></span> Удалить', ['class' => 'btn btn-danger']);
        $form .= Html::endForm();
        echo $form;
        ?>
        <br>
        <?php
        $form = Html::beginForm(['environment/index'], 'get');
        $form .= Html::submitButton('<i class="fa fa-reply"></i> Отмена', ['class' => 'btn btn-success']);
        $form .= Html::endForm();
        echo $form;
        ?>


    </div>

</div>