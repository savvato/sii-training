<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use common\models\BusinessEnvironment;
$this->title = 'Категории делового окружения';
$сategoriesHtml = Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить категорию', ['environment/create'], ['class' => 'btn btn-success']);
$сategoriesHtml .= "<br><br>";
$dataProvider = new ActiveDataProvider([
    'query' => BusinessEnvironment::find(),
]);
$сategoriesHtml .= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'name',
        [
            'class' => 'yii\grid\DataColumn',
            'attribute' => 'main',
            'content' => function($model, $key, $index, $column){
                if ($model->main == 1)
                {
                    return '<span class="label label-success">Основная</span>';
                }
                else
                {
                    return '<span class="label label-primary">Дополнительная</span>';
                }
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return Html::a("<span class='glyphicon glyphicon-trash'></span>", $url, ['title' => 'Удалить']);
                }
            ]
        ],
    ],
    'tableOptions' => ['class' => 'table table-bordered table-hover dataTable'],
    'summary' => ''
]);
?>
<div class="container-fluid">
    <h1>Категории делового окружения</h1>
    <?= $сategoriesHtml ?>
</div>