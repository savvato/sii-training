<?php
use yii\helpers\Html;

$this->title = 'Создать новую категорию';
?>
<div class="container-fluid">
    <h1>Создать новую категорию</h1>

    <?php

    $mainForm = Html::beginForm(['environment/create'], 'post');
    $mainForm .= "<br>";
    $mainForm .= "<div class='form-group'>";
    $mainForm .= Html::label('Название категории:', ['class' => 'control-label']);
    $mainForm .= Html::input('text', 'CategoryName', '', ['class' => 'form-control']);
    $mainForm .= "</div class='form-group'>";
    $mainForm .= "<div class='form-group'>";
    $mainForm .= Html::label('Статус категории:', ['class' => 'control-label']);
    $mainForm .= Html::dropDownList('Main', 0, [1 => 'Основная', 0 => 'Дополнительная'], ['class' => 'form-control']);
    $mainForm .= "</div class='form-group'>";
    $mainForm .= Html::submitButton('<i class="glyphicon glyphicon-plus"></i> Создать', ['class' => 'btn btn-success', 'name' => 'contact-button']);
    $mainForm .= Html::endForm();
    echo $mainForm;
    ?>
</div>
