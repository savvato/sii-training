<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Загрузить изображение';
?>
<div class="container-fluid">
    <h1>Загрузить изображение</h1>
    <?php
    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']])
    ?>
    <?= $form->field($model, 'imageFiles[]')->fileInput(['class' => 'form-control', 'multiple' => true]) ?>
    <?= Html::submitButton('<span class="glyphicon glyphicon-download"></span> Загрузить изображения', ['class' => 'btn btn-success']); ?>
    <?php ActiveForm::end() ?>
</div>


