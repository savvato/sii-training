<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Удалить документ';
?>
<div class="container-fluid">
    <h1><?= $document->full_name ?></h1>
    <p class="text-danger">Вы действительно хотите удалить данный документ?</p>
    <br>
    <p><b>Краткое название: </b><?= $document->short_name ?></p>
    <p><b>Полное название: </b><?= $document->full_name ?></p>
    <?= Html::a("<span class='glyphicon glyphicon-eye-open'></span> Просмотреть документ", '.'.$document->url, ['class' => 'btn btn-default', 'target' => '_blank']) ?>
    <br>
    <br>
    <br>
    <div class="row-fluid">
        <?php
        $form = Html::beginForm(['files/delete-document', 'id' => $document->id], 'post');
        $form .= Html::submitButton('<span class="glyphicon glyphicon-trash"></span> Удалить', ['class' => 'btn btn-danger']);
        $form .= Html::endForm();
        echo $form;
        ?>
        <br>
        <?php
        $form = Html::beginForm(['files/document'], 'get');
        $form .= Html::submitButton('<i class="fa fa-reply"></i> Отмена', ['class' => 'btn btn-success']);
        $form .= Html::endForm();
        echo $form;
        ?>
    </div>

</div>