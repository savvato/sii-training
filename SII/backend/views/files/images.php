<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\FileHelper;
$files = FileHelper::findFiles('../images');
for($i = 0; $i <= count($files) - 1; $i++){
    $files[$i] = strtr($files[$i], ["\\" => "/"]);
}
$this->title = 'Изображения';

use yii\data\ArrayDataProvider;



$provider = new ArrayDataProvider([
    'allModels' => $files,
    'pagination' => [
        'pageSize' => 9,
    ]
]);


?>
<div class="container-fluid">
    <h1>Изображения</h1>
    <div class="container-fluid">
        <?= Html::a("<span class='glyphicon glyphicon-download'></span> Загрузить изображение", ['/files/images-load',], ['class' => 'btn btn-success']) ?>

    </div>
    <br><br>
        <?php
        echo ListView::widget([
            'dataProvider' => $provider,
            'itemView' => function ($model, $key, $index, $widget)
            {
                ?>
                <div class="col-md-4 well">
                    <img src="<?= $model ?>" class="img-responsive" style="height: 200px">
                    <br>
                    <?= Html::a("<span class='glyphicon glyphicon-eye-open'></span> Посмотреть изображение", $model, ['class' => 'btn btn-default', 'target' => '_blank', 'style' => 'width: 100%']) ?>
                    <br>
                    <br>
                    <?= Html::a("<span class='glyphicon glyphicon-unchecked'></span> Установить как логотип веб-ресурса", ['settings/logo', 'file' => $model], ['class' => 'btn btn-primary', 'style' => 'width: 100%']) ?>
                    <br>
                    <br>
                    <?= Html::a("<span class='glyphicon glyphicon-trash'></span> Удалить изображение", ['files/image-delete', 'file' => $model], ['class' => 'btn btn-danger', 'style' => 'width: 100%']) ?>
                </div>
                <?php
            },
            'layout' => "<div class=\"container-fluid\" style=\"margin-top: 30px\">\n{items}</div><br><br><div class='container-fluid'>{summary}\n<br>{pager}</div>"
        ]);
        ?>

</div>


