<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use common\models\Documents;
use yii\helpers\Url;
$this->title = 'Документы';
?>
<div class="container-fluid">
    <h1>Документы</h1>
    <br>
    <?php
    $form = Html::a("<span class='glyphicon glyphicon-download'></span> Загрузить документ", ['files/document-upload'], ['class' => 'btn btn-success']);
    $form .= "<br>";
    $form .= "<br>";
    $dataProvider = new ActiveDataProvider([
        'query' => Documents::find(),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);
    $form .= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'short_name',
            'full_name',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function($url, $model, $key) {
                        return Html::a("<span class='glyphicon glyphicon-eye-open'></span>", '.' . $model->url, ['target' => '_blank']);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a("<span class='glyphicon glyphicon-pencil'></span>", ['/files/update-document', 'id' => $model->id]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a("<span class='glyphicon glyphicon-trash'></span>", ['/files/delete-document', 'id' => $model->id]);
                    }
                ]
            ],
        ],
        'tableOptions' => ['class' => 'table table-bordered table-hover dataTable'],
        'pager' => ['options' => ['class' => 'pagination no-margin pull-right']]
    ]);
    echo $form;
    ?>
</div>


