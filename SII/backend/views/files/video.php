<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Загрузить видео';
?>
<div class="container-fluid">
    <h1>Загрузить видео</h1>
    <?php
    $mainForm = Html::beginForm(['files/video-load'], 'post');
    $mainForm .= "<br>";
    $mainForm .= "<div class='form-group'>";
    $mainForm .= Html::label('Название видео:', ['class' => 'control-label']);
    $mainForm .= Html::input('text', 'name', '', ['class' => 'form-control']);
    $mainForm .= "</div class='form-group'>";
    $mainForm .= "<div class='form-group'>";
    $mainForm .= Html::label('Код для вставки видео:', ['class' => 'control-label']);
    $mainForm .= Html::textarea('code', '', ['class' => 'form-control', 'rows' => 3]);
    $mainForm .= "</div class='form-group'>";
    $mainForm .= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Сохранить', ['class' => 'btn btn-success', 'name' => 'contact-button']);
    $mainForm .= Html::endForm();
    echo $mainForm;
    ?>
</div>


