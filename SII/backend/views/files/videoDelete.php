<?php
use yii\helpers\Html;
$this->title = 'Удалить видео';
?>
<div class="container-fluid">
    <h1><?= $video->name ?></h1>
    <p class="text-danger">Вы действительно хотите удалить данное видео?</p>
    <br>
    <p><b>Название: </b><?= $video->name ?></p>
    <br>
    <?= $video->code ?>
    <br>
    <div class="row-fluid">
            <?php
            $form = Html::beginForm(['files/video-delete', 'id' => $video->id], 'post');
            $form .= Html::submitButton('<span class="glyphicon glyphicon-trash"></span> Удалить', ['class' => 'btn btn-danger']);
            $form .= Html::endForm();
            echo $form;
            ?>
			<br>
            <?php
            $form = Html::beginForm(['files/video'], 'get');
            $form .= Html::submitButton('<i class="fa fa-reply"></i> Отмена', ['class' => 'btn btn-success']);
            $form .= Html::endForm();
            echo $form;
            ?>
    </div>

</div>