<?php
use yii\helpers\Html;
$this->title = 'Изменить документ';
?>
<div class="container-fluid">
    <h1><?= $document->full_name ?></h1>
    <?= Html::a("<span class='glyphicon glyphicon-eye-open'></span> Просмотреть документ", '.'.$document->url, ['class' => 'btn btn-default', 'target' => '_blank']) ?>
    <?php
    $form = Html::beginForm(['files/update-document', 'id' => $document->id], 'post');
    $form .= "<br>";
    $form .= "<div class='form-group'>";
    $form .= Html::label('Краткое название документа:', ['class' => 'control-label']);
    $form .= Html::input('text', 'ShortName', $document->short_name, ['class' => 'form-control']);
    $form .= "</div class='form-group'>";
    $form .= "<br>";
    $form .= "<div class='form-group'>";
    $form .= Html::label('Полное наименование документа:', ['class' => 'control-label']);
    $form .= Html::input('text', 'FullName', $document->full_name, ['class' => 'form-control']);
    $form .= "</div class='form-group'>";
    $form .= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Сохранить изменения', ['class' => 'btn btn-success']);
    $form .= Html::endForm();
    echo $form;
    ?>
</div>