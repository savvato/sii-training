<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Загрузить документ';
?>
<div class="container-fluid">
    <h1>Загрузить документ</h1>
    <?php
        $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']])
    ?>
    <?= $form->field($model, 'fullDocName')->textInput(['class' => 'form-control']) ?>
    <?= $form->field($model, 'shortDocName')->textInput(['class' => 'form-control']) ?>
    <?= $form->field($model, 'file')->fileInput(['class' => 'form-control']) ?>
    <?= Html::submitButton('<span class="glyphicon glyphicon-download"></span> Загрузить документ', ['class' => 'btn btn-success']); ?>
    <?php ActiveForm::end() ?>
</div>


