<?php
use yii\helpers\Html;
$this->title = 'Видео';
?>
<div class="container-fluid">
    <h1>Видео</h1>
    <?= Html::a("<span class='glyphicon glyphicon-download'></span> Добавить видео", ['/files/video-load',], ['class' => 'btn btn-success']) ?>
        <div class="container-fluid" style="margin-top: 30px">
            <?php foreach($videos as $video): ?>
                <div class="row-fluid well" style="text-align: center">
                    <h3 style="text-align: center"><?= $video->name ?></h3>
                    <?= $video->code ?>
                    <?= Html::a
                    (
                        "<span class='glyphicon glyphicon-pencil'></span> Редактировать данные",
                        ['/files/video-update', 'id' => $video->id],
                        ['class' => 'btn btn-default', 'style' => 'width: 100%']
                    ); ?>
                    <br>
                    <br>
                    <?= Html::a
                    (
                        "<span class='glyphicon glyphicon-trash'></span> Удалить видео",
                        ['/files/video-delete', 'id' => $video->id],
                        ['class' => 'btn btn-danger', 'style' => 'width: 100%']
                    ); ?>
                </div>
            <?php endforeach; ?>
    </div>

</div>
