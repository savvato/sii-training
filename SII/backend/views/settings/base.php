<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\FileHelper;
$this->title = 'Базовые настройки системы';
?>
<div class="container-fluid">
    <h1>Базовые настройки системы</h1>
    <?=
        Html::beginForm(['settings/base'], 'post');
    ?>
    <br>
    <div class='form-group'>
        <?= Html::label('Название веб-ресурса:', ['class' => 'control-label']) ?>
        <?= Html::input('text', 'siteName', $settings['siteName'], ['class' => 'form-control']) ?>
    </div class='form-group'>
    <div class='form-group'>
        <?= Html::label('Статус веб-ресурса:', ['class' => 'control-label']) ?>
        <?= Html::dropDownList('siteStatus',
            $settings['siteStatus'],
            [
                'Available' => 'Доступен',
                'NotAvailable' => 'Недоступен'
            ],
            ['class' => 'form-control'])
        ?>
    </div class='form-group'>
    <?= Html::submitButton('<i class="fa fa-floppy-o"></i> Сохранить настройки',
        ['class' => 'btn btn-success'])
    ?>
    <?= Html::endForm() ?>

</div>



