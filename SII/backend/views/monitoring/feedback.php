<?php
use yii\widgets\DetailView;
use common\models\BusinessEnvironment;
$this->title = 'Сообщение обратной связи';
?>
<div class="container-fluid" style="padding-top: 15px;">
    <?= \yii\helpers\Html::a('<i class="glyphicon glyphicon-arrow-left"></i> Назад', ['monitoring/feedbacks'], ['class' => 'btn btn-default']) ?>
    <br><br>
    <?php
    echo DetailView::widget([
        'model' => $feedback,
        'attributes' => [
            'name',
            'category',
            'email',
            'subject',
            'body',
            [
                'attribute' => 'date',
                'format' => ['date', 'php:Y-m-d H:i:s']
            ],
        ],
    ]);
    ?>
</div>