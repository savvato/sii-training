<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use common\models\Feedback;
use common\models\BusinessEnvironment;
$this->title = 'Сообщения обратной связи';
?>
<div class="container-fluid">
    <h1>Обратная связь</h1>

    <p>Непрочитано <strong><?= $countOfNotReadedFeedbacks ?></strong> сообщений. </p>

    <?php
    $dataProvider = new ActiveDataProvider([
        'query' => Feedback::find()->orderBy(['date' => SORT_DESC]),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'category',
                'format' => 'text',
            ],
            [
                'attribute' => 'date',
                'content' => function($model, $key, $index, $column)
                {
                    return Yii::$app->formatter->asDatetime(date_create($model->date), 'php: Y-m-d H:i:s');
                }
            ],
            'subject',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{show}',
                'buttons' => [
                    'show' => function ($url, $model, $key) {
                        return Html::a("<span class='glyphicon glyphicon-eye-open'></span>", ['monitoring/show-feedback', 'id' => $model->id]);
                    }
                ]
            ],
        ],
        'rowOptions' => function($model, $key, $index, $grid)
        {
            if (!$model->is_readed){
                return [
                    'class' => 'success'
                ];
            }
            else return [];
        },
        'tableOptions' => ['class' => 'table table-bordered table-hover dataTable'],
        'pager' => ['options' => ['class' => 'pagination no-margin pull-right']]
    ]);
    ?>
</div>



