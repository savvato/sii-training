<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\FileHelper;
$this->title = 'Страница "Контакты"';
$files = FileHelper::findFiles('../images');
$list = "";
for($i = 0; $i <= count($files) - 1; $i++){
    $file = strtr($files[$i], "\\", "/");
    $list .= "{title: '".$file."', value: '".$file."'},";
}
$this->registerJs("tinymce.init({
        selector: '#redactor',
        plugins: 'hr table advlist colorpicker textcolor image lists charmap preview anchor wordcount visualblocks visualchars code fullscreen insertdatetime',
        tools: 'inserttable',
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons',
        language: 'ru',
        image_advtab: true,
        image_list: [
            " . $list ."
        ]
    });", View::POS_READY);
?>
<div class="container-fluid">

    <h1>Контакты</h1>
    <?php
    $form = Html::beginForm(['pages/contacts'], 'post');
    $form .= "<br>";
    $form .= "<div class='form-group'>";
    $form .= Html::textarea('Page', $page, ['id' => 'redactor', 'rows' => '11']);
    $form .= "</div class='form-group'>";
    $form .= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Сохранить изменения', ['class' => 'btn btn-success']);
    $form .= Html::endForm();
    echo $form;
    ?>

</div>




