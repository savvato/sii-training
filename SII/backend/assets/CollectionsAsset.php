<?php

namespace backend\assets;

use yii\web\AssetBundle;

class CollectionsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/AdminLTE.min.css',
        'css/skin-blue.min.css',
        'css/editor.css',
        'css/jBox.css',
        YII_DEBUG ? 'css/font-awesome.css' : 'css/font-awesome.min.css'
    ];
    public $js = [
        'js/DataCollectionsApp/app.js',
        'js/DataCollectionsApp/controllers/MainController.js',
        'js/DataCollectionsApp/controllers/CreatingCollectionController.js',
        'js/DataCollectionsApp/controllers/EditingCollectionController.js',
        'js/jBox.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'backend\assets\AngularAsset'
    ];
}