<?php

namespace backend\assets;

use yii\web\AssetBundle;

class ScenarioAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/AdminLTE.min.css',
        'css/skin-blue.min.css',
        'css/editor.css',
        'css/jBox.css',
        '../css/blockstyles.css',
        YII_DEBUG ? 'css/font-awesome.css' : 'css/font-awesome.min.css',
        YII_DEBUG ? 'css/medium-editor.css' : 'css/medium-editor.min.css',
        YII_DEBUG ? 'css/default.css' : 'css/default.min.css',
    ];
    public $js = [
        YII_DEBUG ? 'js/medium-editor.js' : 'js/medium-editor.min.js',
        'js/ScenarioApp/app.js',
        'js/ScenarioApp/controllers/MainController.js',
        'js/ScenarioApp/controllers/ScenarioController.js',
        'js/ScenarioApp/controllers/SettingsController.js',
        'js/ScenarioApp/controllers/WorkspaceController.js',
        YII_DEBUG ? 'js/angularLibs/angular-medium-editor.js' : 'js/angularLibs/angular-medium-editor.min.js',
        '../js/blocksScripts.js',
        'js/jBox.min.js',
        'js/editor.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'backend\assets\AngularAsset'
    ];
}