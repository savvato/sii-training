<?php

namespace backend\assets;

use yii\web\AssetBundle;

class AngularAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        YII_DEBUG ? 'css/select.css' : 'css/select.min.css',
        'css/angular-ui-tree.min.css'
    ];
    public $js = [
        YII_DEBUG ? 'js/angularLibs/angular.js' : 'js/angularLibs/angular.min.js',
        YII_DEBUG ? 'js/angularLibs/angular-ui-tree.js' : 'js/angularLibs/angular-ui-tree.min.js',
        YII_DEBUG ? 'js/angularLibs/angular-ui-select.js' : 'js/angularLibs/angular-ui-select.min.js',

        //'js/angularApp/angularLibs/angular-ui-tinymce.js',
    ];
    public $depends = [

    ];
}