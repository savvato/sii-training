app.controller('MainController',
    [
        '$scope',
        '$http',
        '$document',
        function($scope, $http, $document){
            $scope.load = function(){
                $http.get($scope.pathToLoad)
                    .success(function(data, status, headers, config){
                        $scope.collections = data;
                        new jBox('Notice', {
                            title: 'Коллекции успешно загружены',
                            content: 'Код статуса: ' + status,
                            theme: 'Notice-red',
                            autoClose: 2000,
                            color: 'green'
                        });
                    })
                    .error(function(data, status, headers, config){
                        new jBox('Notice', {
                            title: 'Ошибка при загрузке коллекций',
                            content: 'Код статуса: ' + status,
                            theme: 'Notice-red',
                            autoClose: 2000,
                            color: 'red'
                        })
                    })
            };

            $document.ready(function(){
                $scope.load();
            });

            $scope.editingCollection = function(collection){
                $scope.currentCollection = collection;
                $scope.mode = "editingCollection";
            };

            $scope.creatingCollection = function(){
                $scope.currentCollection = {
                    collection_name: "",
                    settings: [],
                    structure: [],
                    content: []
                };
                $scope.mode = "creatingCollection";
            }

        }
    ]
);