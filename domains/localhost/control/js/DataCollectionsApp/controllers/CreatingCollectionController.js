app.controller('CreatingCollectionController',
    [
        '$scope',
        function($scope){
            $scope.fieldTypes = [
                {
                    type_name: "number",
                    name: "Число",
                    icon: "fa-calculator"
                },
                {
                    type_name: "text",
                    name: "Текст",
                    icon: "fa-font"
                },
                {
                    type_name: "date",
                    name: "Календарная дата",
                    icon: "fa-calendar"
                },
                {
                    type_name: "datetime",
                    name: "Дата и время",
                    icon: "fa-clock-o"
                },
                {
                    type_name: "email",
                    name: "Электронная почта",
                    icon: "fa-envelope-o"
                },
                {
                    type_name: "tel",
                    name: "Телефонный номер",
                    icon: "fa-phone"
                },
                {
                    type_name: "image",
                    name: "Изображение",
                    icon: "fa-picture-o"
                }
            ];

            $scope.translit = function(text){
                var transl = [];
                transl['А']='A';     transl['а']='a';
                transl['Б']='B';     transl['б']='b';
                transl['В']='V';     transl['в']='v';
                transl['Г']='G';     transl['г']='g';
                transl['Д']='D';     transl['д']='d';
                transl['Е']='E';     transl['е']='e';
                transl['Ё']='Yo';    transl['ё']='yo';
                transl['Ж']='Zh';    transl['ж']='zh';
                transl['З']='Z';     transl['з']='z';
                transl['И']='I';     transl['и']='i';
                transl['Й']='J';     transl['й']='j';
                transl['К']='K';     transl['к']='k';
                transl['Л']='L';     transl['л']='l';
                transl['М']='M';     transl['м']='m';
                transl['Н']='N';     transl['н']='n';
                transl['О']='O';     transl['о']='o';
                transl['П']='P';     transl['п']='p';
                transl['Р']='R';     transl['р']='r';
                transl['С']='S';     transl['с']='s';
                transl['Т']='T';     transl['т']='t';
                transl['У']='U';     transl['у']='u';
                transl['Ф']='F';     transl['ф']='f';
                transl['Х']='X';     transl['х']='x';
                transl['Ц']='C';     transl['ц']='c';
                transl['Ч']='Ch';    transl['ч']='ch';
                transl['Ш']='Sh';    transl['ш']='sh';
                transl['Щ']='Shh';    transl['щ']='shh';
                transl['Ъ']='"';     transl['ъ']='"';
                transl['Ы']='Y\'';    transl['ы']='y\'';
                transl['Ь']='\'';    transl['ь']='\'';
                transl['Э']='E\'';    transl['э']='e\'';
                transl['Ю']='Yu';    transl['ю']='yu';
                transl['Я']='Ya';    transl['я']='ya';
                transl[' ']='_';
                var result='';
                for(i=0;i<text.length;i++) {
                    if(transl[text[i]]!=undefined) { result+=transl[text[i]]; }
                    else { result+=text[i]; }
                }

                return result;
            };

            $scope.newField = null;

            $scope.addingField = false;

            $scope.showAddFieldForm = function() {
                $scope.addingField = true;
                $scope.newField = {
                    name: "",
                    title: "",
                    type: {
                        type_name: "text",
                        name: "Текст",
                        icon: "fa-font"
                    },
                    required: false
                };
            };

            $scope.closeAddFieldForm = function() {
                $scope.addingField = false;
                $scope.newField = null;
            };

            $scope.addField = function() {
                if ($scope.newField.title != ""){
                    $scope.newField.name = $scope.translit($scope.newField.title);
                    $scope.currentCollection.structure.push($scope.newField);
                    $scope.newField = null;
                    $scope.addingField = false;
                }
            };

            $scope.createCollection = function(){
                if ($scope.currentCollection.collection_name != "" && $scope.currentCollection.structure.length != 0){
                    var collection = angular.toJson($scope.currentCollection);
                    $.post($scope.pathToCreate, {collection: collection}, function(data, status){
                        new jBox('Notice', {
                            title: 'Коллекция успешно создана',
                            content: 'Код статуса ответа: ' + status,
                            theme: 'Notice-red',
                            autoClose: 2000,
                            color: 'green'
                        });
                        $scope.load();
                        $scope.mode = null;
                        $scope.currentCollection = null;
                    });
                }
            };

            $scope.deleteField = function(fieldId) {
                $scope.currentCollection.structure.splice(fieldId, 1);
            }
        }
    ]
);