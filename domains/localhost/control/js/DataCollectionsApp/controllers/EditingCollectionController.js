app.controller('EditingCollectionController',
    [
        '$scope',
        '$http',
        function($scope, $http){
            $scope.showAddRecordForm = false;

            $scope.newRecord = null;

            $scope.addingNewRecord = function() {
                if ($scope.showAddRecordForm == false){
                    $scope.showAddRecordForm = true;
                    $scope.newRecord = {};
                    $('#collapseAdd').collapse('show');
                }
                else {
                    $scope.showAddRecordForm = false;
                    $scope.newRecord = null;
                    $('#collapseAdd').collapse('hide');
                }
            };

            $scope.closeAddRecordForm = function() {
                $scope.showAddRecordForm = false;
                $scope.newRecord = null;
                $('#collapseAdd').collapse('hide');
            };

            $scope.addRecord = function(form) {
                if (form.$valid) {
                    $scope.currentCollection.content.push($scope.newRecord);
                    $scope.closeAddRecordForm();
                }
            };

            $scope.saveCollection = function() {
                var collection = angular.toJson($scope.currentCollection);
                $.post($scope.pathToSave, {collection: collection}, function(data, status){
                    new jBox('Notice', {
                        title: 'Коллекция успешно сохранена',
                        content: 'Код статуса ответа: ' + status,
                        theme: 'Notice-red',
                        autoClose: 2000,
                        color: 'green'
                    });
                    $scope.load();
                });
            };

            $scope.deleteCollection = function() {
                if (confirm('Вы действительно хотите удалить эту коллекцию данных?')){
                    $.post($scope.pathToDelete, {collection_name: $scope.currentCollection.collection_name}, function(data, status){
                        new jBox('Notice', {
                            title: 'Коллекция успешно удалена',
                            content: 'Код статуса ответа: ' + status,
                            theme: 'Notice-red',
                            autoClose: 2000,
                            color: 'green'
                        });
                        $scope.load();
                    });
                }
            };

            $scope.deleteRecord = function (recordID) {
                $scope.currentCollection.content.splice(recordID, 1);
            }
        }
    ]
);