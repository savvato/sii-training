var app = angular.module('DataCollectionsApp', ['ui.tree', 'ui.select']);

//Регистрация CSRF-токена для запросов
app.config([
    "$httpProvider", function($httpProvider) {
        $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
        //$httpProvider.defaults.headers.post['Content-Type'] = "application/x-www-form-urlencoded";
    }
]);
