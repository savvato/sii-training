app.controller('ScenarioController',
    [
        '$scope',
        '$currentNodes',
        function($scope, $currentNodes){
            //Добавление группы в сценарии
            $scope.addGroup = function(){
                $scope.scenario.push(
                    {
                        group_name: 'Группа ' + ($scope.scenario.length + 1),
                        steps: []
                    }
                );
            };

            //Добавление шага к сценарию
            $scope.addStep = function(){
                if($scope.currentGroup != null){
                    $scope.currentGroup.steps.push(
                        {
                            step_name: 'Шаг ' + ($scope.currentGroup.steps.length + 1),
                            type: "simplePage",
                            content_structure: []
                        }
                    );
                }
            };

            //Установка нужной формы для управления структурой сценария
            $scope.getForm = function(){
                if($currentNodes.getStatus() == true){
                    return "./js/ScenarioApp/forms/formGroup.html"
                }
                else if ($currentNodes.getStatus() == false){
                    return "./js/ScenarioApp/forms/formStep.html"
                }
                else {
                    return null;
                }
            };

            //Установка текущей группы для редактирования в форме работы со сценарием
            $scope.editGroup = function(scope){
                $scope.currentStep = null;
                $currentNodes.setStep(null);
                $scope.currentGroup = scope.$modelValue;
                $currentNodes.setGroup(scope.$index);
            };

            //Установка текущего шага для редактирования в форме работы со сценарием
            $scope.editStep = function(scope){
                $scope.currentGroup = $scope.scenario[scope.$parentNodeScope.$index];
                $currentNodes.setGroup(scope.$parentNodeScope.$index);
                $scope.currentStep = scope.$modelValue;
                $currentNodes.setStep(scope.$index);
            };

            /*Функция применяющая структуру данных, соответствующую типу страницы*/
            $scope.changePageType = function(){
                switch ($scope.currentStep.type){
                    case 'simplePage':
                        $scope.currentStep.content_structure = [];
                        break;
                    case 'sliderPage':
                        $scope.currentStep.content_structure = [
                            {
                                "block" : {
                                    "type" : "definitiveCard",
                                    "content" : {
                                        "title" : "Заголовок",
                                        "second_title" : "Заголовок 2",
                                        "text" : "Текст текст",
                                        "clarification" : "Пояснение, пояснение"
                                    },
                                    "settings" : {
                                        "main_image" : {
                                            "url" : null
                                        },
                                        "image" : {
                                            "url" : null
                                        }
                                    }
                                },
                                "content_structure" : []
                            }
                        ];
                        break;
                    case 'feedbackPage':
                        $scope.currentStep.content_structure = {
                            "introduction" : "Если у вас есть вопросы, пожалуйста, заполните следующую форму для связи с нами. Спасибо."
                        };
                        break;
                    case 'documentPage':
                        $scope.currentStep.content_structure = [
                            {
                                "block" : {
                                    "type" : "definitiveCard",
                                    "content" : {
                                        "title" : "Заголовок",
                                        "second_title" : "Заголовок 2",
                                        "text" : "Текст текст",
                                        "clarification" : "Пояснение, пояснение"
                                    },
                                    "settings" : {
                                        "main_image" : {
                                            "url" : null
                                        },
                                        "image" : {
                                            "url" : null
                                        }
                                    }
                                },
                                "content_structure" : []
                            }
                        ];
                        break;
                }
            };

            //Удаление группы
            $scope.removeGroup = function (scope) {
                if (confirm("Вы действительно хотите удалить группу \"" + scope.$modelValue.group_name + "\"? Это приведет к удалению всех данных, связанных с этой группой.")){
                    scope.remove();
                }
            };

            //Удаление шага
            $scope.removeStep = function (scope) {
                if (confirm("Вы действительно хотите удалить шаг \"" + scope.$modelValue.step_name + "\"? Это приведет к удалению всех данных, связанных с этой страницей.")){
                    scope.remove();
                }
            };

            //Настройки редактора сценария
            $scope.treeOptions = {
                //Обработчик возможности изменения уровня узла
                accept: function(sourceNodeScope, destNodesScope, destIndex) {
                    if (sourceNodeScope.depth() == 2){
                        return destNodesScope.depth() == 1 ;
                    }
                    else {
                        return sourceNodeScope.depth() > destNodesScope.depth();
                    }

                }
            };

        }
    ]
);