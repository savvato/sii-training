app.controller('MainController',
    [
        '$scope',
        '$http',
        '$document',
        '$currentNodes',
        function ( $scope, $http, $document, $currentNodes) {
            //Загрузка сценария с сервера
            $scope.load = function(){
                $http.get($scope.pathToLoad)
                    .success(function(data, status, headers, config){
                        $scope.data = data;
                        $scope.categoryName = $scope.data.categoryName;
                        $scope.scenario = $scope.data.scenario.manual_scenario;
                        $scope.settings = $scope.data.scenario.settings;
                        $scope.currentPage = $scope.scenario[0].steps[0];
                        new jBox('Notice', {
                            title: 'Загрузка сценария прошла успешно',
                            content: 'Код статуса: ' + status,
                            theme: 'Notice-red',
                            autoClose: 2000,
                            color: 'green'
                        });
                    })
                    .error(function(data, status, headers, config){
                        new jBox('Notice', {
                            title: 'Ошибка при загрузке сценария',
                            content: 'Код статуса: ' + status,
                            theme: 'Notice-red',
                            autoClose: 2000,
                            color: 'red'
                        })
                    })
            };

            $scope.collectionsLoad = function(){
                $http.get($scope.pathToCollectionsLoad)
                    .success(function(data, status, headers, config){
                        $scope.collections = data;
                        new jBox('Notice', {
                            title: 'Коллекции успешно загружены',
                            content: 'Код статуса: ' + status,
                            theme: 'Notice-red',
                            autoClose: 2000,
                            color: 'green'
                        });
                    })
                    .error(function(data, status, headers, config){
                        new jBox('Notice', {
                            title: 'Ошибка при загрузке коллекций',
                            content: 'Код статуса: ' + status,
                            theme: 'Notice-red',
                            autoClose: 2000,
                            color: 'red'
                        })
                    })
            };

            $document.ready(function(){
                $scope.load();
                $scope.collectionsLoad();
            });//Запуск функции загрузки сценария и коллекций данных

            //Отправка сценария на сервер для сохранения
            $scope.save = function() {
                /*$http.post($scope.pathToSave, {scenario: angular.toJson($scope.scenario)})
                    .success(function(data, status, headers, config) {
                        new jBox('Notice', {
                            title: 'Сохранение прошло успешно',
                            content: 'Код статуса: ' + status,
                            theme: 'Notice-red',
                            autoClose: 2000,
                            color: 'green'
                        });
                    })
                    .error(function(data, status, headers, config) {
                        new jBox('Notice', {
                            title: 'Ошибка при сохранении',
                            content: 'Код статуса: ' + status,
                            theme: 'Notice-red',
                            autoClose: 2000,
                            color: 'red'
                        })
                    });*/
                $.post($scope.pathToSave, {scenario: angular.toJson($scope.scenario), settings: angular.toJson($scope.settings)}, function(data, status){
                    new jBox('Notice', {
                        title: 'Данные успешно отправлены',
                        content: 'Код статуса ответа: ' + status,
                        theme: 'Notice-red',
                        autoClose: 2000,
                        color: 'green'
                    });
                });
            };

            $scope.currentGroupID = $currentNodes.getGroup();
            $scope.currentStepID = $currentNodes.getStep();

            /*Устанавливается текущая страница редактора*/
            $scope.setCurrentPage = function(scope){
                $currentNodes.setGroup(scope.$parentNodeScope.$index);
                $scope.currentPage = scope.$modelValue;
                $currentNodes.setStep(scope.$index);
                $('#select-modal').modal('hide');
            };

            $scope.setCurrentCollection = function(collection){
                $scope.currentCollection = collection;
            };

            $scope.mediumBindOptions = {
                autoLink: true,
                placeholder: false,
                toolbar: {
                    buttons:
                        [
                            'bold',
                            'italic',
                            'underline',
                            'strikethrough',
                            'subscript',
                            'superscript',
                            'anchor',
                            'orderedlist',
                            'unorderedlist',
                            'justifyLeft',
                            'justifyCenter',
                            'justifyRight',
                            'justifyFull',
                            'h1',
                            'h2',
                            'h3',
                            'h4',
                            'h5',
                            'h6',
                            'removeFormat'
                        ]
                }
            };
            $scope.mediumSeparatorOptions = {
                autoLink: true,
                placeholder: false,
                toolbar: {
                    buttons:
                        [
                            'bold',
                            'italic',
                            'underline',
                            'strikethrough',
                            'subscript',
                            'superscript',
                            'anchor',
                            'removeFormat'
                        ]
                }
            };

        }
    ]
);