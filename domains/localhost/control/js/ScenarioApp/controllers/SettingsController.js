app.controller('SettingsController',
    [
        '$scope',
        function($scope){
            //Функция, вызываемая при изменении settings.hasVideo
            $scope.onHasVideoChanged = function () {
                if ($scope.settings.hasVideo == false) {
                    $scope.settings.videoID = null;
                }
                else {
                    $scope.settings.videoID = Number($scope.settings.videoID);
                }
            };

            /*Функция, вызываемая при изменении settings.hasSidebarBackground*/
            $scope.onHasSidebarBackgroundChanged = function(){
                if ($scope.settings.hasSidebarBackground == false) {
                    $scope.settings.sidebarBackground = null;
                }
            };
        }
    ]
);