app.controller('WorkspaceController',
    [
        '$scope',
        '$currentNodes',
        function($scope, $currentNodes){
            /*Функция, устанавливающая рабочую структуру данных для функций работающих с разными типами страниц*/
            $scope.getWorkspace = function(){
                var place;
                switch ($scope.currentPage.type){
                    case "simplePage":
                        place = $scope.currentPage;
                        break;
                    case "sliderPage":
                        if($currentNodes.getSlide() == null){
                            $scope.currentSlide = $scope.currentPage.content_structure[0];
                            $currentNodes.setSlide(0);
                            place = $scope.currentSlide;
                        }
                        else {
                            $scope.currentSlide = $scope.currentPage.content_structure[$currentNodes.getSlide()];
                            place = $scope.currentSlide;
                        }
                        break;
                    case "documentPage":
                        if($currentNodes.getSlide() == null){
                            $scope.currentSlide = $scope.currentPage.content_structure[0];
                            $currentNodes.setSlide(0);
                            place = $scope.currentSlide;
                        }
                        else {
                            $scope.currentSlide = $scope.currentPage.content_structure[$currentNodes.getSlide()];
                            place = $scope.currentSlide;
                        }
                        break;
                }
                return place;
            };

            /*Функция, устанавливающая шаблон для блоков*/
            $scope.getTemplate = function (block) {
                switch (block.type){
                    case "separator":
                        return "./js/ScenarioApp/templates/separator.html";
                        break;
                    case "tricks":
                        return "./js/ScenarioApp/templates/tricks.html";
                        break;
                    case "trick":
                        return "./js/ScenarioApp/templates/trick.html";
                        break;
                    case "jumbotronSimple":
                        return "./js/ScenarioApp/templates/jumbotronSimple.html";
                        break;
                    case "jumbotronMultiple":
                        return "./js/ScenarioApp/templates/jumbotronMultiple.html";
                        break;
                    case "jumboBlock":
                        return "./js/ScenarioApp/templates/jumboBlock.html";
                        break;
                    case "definitiveCard":
                        return "./js/ScenarioApp/templates/definitiveCard.html";
                        break;
                    case "definitivePhotoCard":
                        return "./js/ScenarioApp/templates/definitivePhotoCard.html";
                        break;
                    case "definitivePhoto":
                        return "./js/ScenarioApp/templates/definitivePhoto.html";
                        break;
                    case "contentCards":
                        return "./js/ScenarioApp/templates/contentCards.html";
                        break;
                    case "contentCard":
                        return "./js/ScenarioApp/templates/contentCard.html";
                        break;
                    case "documentsSection":
                        return "./js/ScenarioApp/templates/documentsSection.html";
                        break;
                }
            };

            $scope.getDocument = function(docID){
                for (var index = 0; index <= $scope.documents.length - 1; index++){
                    if ($scope.documents[index].id == docID){
                        return $scope.documents[index];
                    }
                }
            };

            /*Функция добавления раздела*/
            $scope.addSection = function(sectionType) {
                var place = $scope.getWorkspace();
                switch (sectionType){
                    case "separator":
                        place.content_structure.push(
                            {
                                "type" : "separator",
                                "hasBlocks" : false,
                                "content" : {
                                    "title" : "Текст"
                                },
                                "settings" : {}
                            }
                        );
                        break;
                    case "tricks":
                        place.content_structure.push(
                            {
                                "type" : "tricks",
                                "hasBlocks" : true,
                                "blocks" : [
                                    {
                                        "type" : "trick",
                                        "content" : {
                                            "title" : "Текст"
                                        },
                                        "settings" : {
                                            "annotation" : "Поясняющий текст",
                                            "backgroundImage" : null,
                                            "fontColor" : "#FFFFFF",
                                            "annotationBackgroundColor" : "#3E70BA",
                                            "width": 2
                                        }
                                    }
                                ],
                                "settings" : {
                                    "blocksHeight" : 120
                                }
                            }
                        );
                        break;
                    case "jumbotronSimple":
                        place.content_structure.push(
                            {
                                "type" : "jumbotronSimple",
                                "hasBlocks" : false,
                                "content" : {
                                    "title" : "Заголовок",
                                    "text" : "Текст текст текст текст текст текст текст текст текст"
                                },
                                "settings" : {
                                    "backgroundImage" : {
                                        "url" : null
                                    }
                                }
                            }
                        );
                        break;
                    case "jumbotronMultiple":
                        place.content_structure.push(
                            {
                                "type" : "jumbotronMultiple",
                                "hasBlocks" : true,
                                "blocks" : [
                                    {
                                        "type" : "jumboBlock",
                                        "content" : {
                                            "title" : "Заголовок",
                                            "text" : "Текст Текст Текст Текст Текст Текст "
                                        },
                                        "settings" : {
                                            "width": 3
                                        }
                                    }
                                ],
                                "settings" : {
                                    "backgroundImage" : {
                                        "url" : null
                                    },
                                    "hasIntegration" : false,
                                    "integrationSettings" : {
                                        "collectionName" : "",
                                        "matchingFields" : {
                                            "title": "",
                                            "text" : ""
                                        }
                                    }
                                }
                            }
                        );
                        break;
                    case "contentCards":
                        place.content_structure.push(
                            {
                                "type" : "contentCards",
                                "hasBlocks" : true,
                                "blocks" : [
                                    {
                                        "type" : "contentCard",
                                        "content" : {
                                            "title" : "Заголовок",
                                            "text" : "Текст"
                                        },
                                        "settings" : {
                                            "image" : null,
                                            "imagePlace" : "top",
                                            "blockWidth" : 3,
                                            "imageWidth" : 4,
                                            "contentWidth" : 8
                                        }
                                    }
                                ],
                                "settings" : {
                                    "hasBorders" : true
                                }
                            }
                        );
                        break;
                    case "documentsSection":
                        place.content_structure.push(
                            {
                                "type" : "documentsSection",
                                "hasBlocks" : true,
                                "documents" : []
                            }
                        );
                }
                $('#add-block-modal').modal('hide');
            };
            /*Функция добавления блоков в разделах*/
            $scope.addBlock = function (section){
                switch(section.type){
                    case "tricks":
                        section.blocks.push(
                            {
                                "type" : "trick",
                                "content" : {
                                    "title" : "Текст"
                                },
                                "settings" : {
                                    "annotation" : "Поясняющий текст",
                                    "backgroundImage" : null,
                                    "fontColor" : "#FFFFFF",
                                    "annotationBackgroundColor" : "#3E70BA",
                                    "width": 2
                                }
                            }
                        );
                        break;
                    case "jumbotronMultiple":
                        section.blocks.push(
                            {
                                "type" : "jumboBlock",
                                "content" : {
                                    "title" : "Заголовок",
                                    "text" : "Текст Текст Текст Текст Текст Текст "
                                },
                                "settings" : {
                                    "width": 3
                                }
                            }
                        );
                        break;
                    case "contentCards":
                        section.blocks.push(
                            {
                                "type" : "contentCard",
                                "content" : {
                                    "title" : "Заголовок",
                                    "text" : "Текст"
                                },
                                "settings" : {
                                    "image" : null,
                                    "imagePlace" : "top",
                                    "blockWidth" : 3,
                                    "imageWidth" : 4,
                                    "contentWidth" : 8
                                }
                            }
                        );
                        break;
                }
            };

            /*Функция удаления блоков из секции*/
            $scope.removeBlock = function(section, id) {
                if (confirm("Вы действительно хотите удалить данный контентый блок?")){
                    section.blocks.splice(id, 1);
                }
            };

            /*Функция удаления секционных блоков*/
            $scope.removeSection = function(id){
                if (confirm("Вы действительно хотите удалить данный контентый блок?")){
                    $scope.getWorkspace().content_structure.splice(id, 1);
                }
            };

            /*Функция удаления слайдов*/
            $scope.removeSlide = function(id){
                $scope.currentPage.content_structure.splice(id, 1);
            };

            /*Функция для вывода настроек блока*/
            $scope.getBlockSettings = function(block){
                $scope.currentBlock = block;
                $('#block-settings').modal('show');
            };

            /*Функция для выдачи ссылки на форму настроек соответствующего блока или секции*/
            $scope.getSettingsForm = function(){
                switch($scope.currentBlock.type){
                    case "separator":
                        return "./js/ScenarioApp/forms/separatorForm.html";
                        break;
                    case "tricks":
                        return "./js/ScenarioApp/forms/tricksForm.html";
                        break;
                    case "trick":
                        return "./js/ScenarioApp/forms/trickForm.html";
                        break;
                    case "jumbotronSimple":
                        return "./js/ScenarioApp/forms/jumbotronSimpleForm.html";
                        break;
                    case "jumbotronMultiple":
                        return "./js/ScenarioApp/forms/jumbotronMultipleForm.html";
                        break;
                    case "jumboBlock":
                        return "./js/ScenarioApp/forms/jumboBlockForm.html";
                        break;
                    case "definitiveCard":
                        return "./js/ScenarioApp/forms/definitiveCardForm.html";
                        break;
                    case "definitivePhotoCard":
                        return "./js/ScenarioApp/forms/definitiveCardForm.html";
                        break;
                    case "definitivePhoto":
                        return "./js/ScenarioApp/forms/definitiveCardForm.html";
                        break;
                    case "contentCards":
                        return "./js/ScenarioApp/forms/contentCardsForm.html";
                        break;
                    case "contentCard":
                        return "./js/ScenarioApp/forms/contentCardForm.html";
                        break;
                    case "documentsSection":
                        return "./js/ScenarioApp/forms/documentsSectionForm.html";
                        break;
                }
            };

            //Функция поднятия секции
            $scope.sectionUp = function(sectionIndexToUp){
                if (sectionIndexToUp != 0 ){
                    var temp = $scope.getWorkspace().content_structure[sectionIndexToUp];
                    $scope.getWorkspace().content_structure[sectionIndexToUp] = $scope.getWorkspace().content_structure[sectionIndexToUp - 1];
                    $scope.getWorkspace().content_structure[sectionIndexToUp - 1] = temp;
                }
            };

            //Функция спуска секции
            $scope.sectionDown = function(sectionIndexToDown){
                if (sectionIndexToDown != $scope.getWorkspace().content_structure.length - 1){
                    var temp = $scope.getWorkspace().content_structure[sectionIndexToDown];
                    $scope.getWorkspace().content_structure[sectionIndexToDown] = $scope.getWorkspace().content_structure[sectionIndexToDown + 1];
                    $scope.getWorkspace().content_structure[sectionIndexToDown + 1] = temp;
                }
            };

            //Функция сдвигает блок влево
            $scope.blockLeft = function(section, blockIndexToLeft){
                if (blockIndexToLeft != 0 ){
                    var temp = section.blocks[blockIndexToLeft];
                    section.blocks[blockIndexToLeft] = section.blocks[blockIndexToLeft - 1];
                    section.blocks[blockIndexToLeft - 1] = temp;
                }
            };

            //Функция сдвигает блок вправо
            $scope.blockRight = function(section, blockIndexToRight){
                if (blockIndexToRight != section.blocks.length - 1) {
                    var temp = section.blocks[blockIndexToRight];
                    section.blocks[blockIndexToRight] = section.blocks[blockIndexToRight + 1];
                    section.blocks[blockIndexToRight + 1] = temp;
                }
            };

            $scope.currentSlideID = $currentNodes.getSlide();

            /*Функция, устанавливающая текущий слайд на слайдерной странице*/
            $scope.setCurrentSlide = function(slideID){
                $currentNodes.setSlide(slideID);
                $scope.currentSlideID = $currentNodes.getSlide();
            };

            /*Функция добавляющая слайд на слайдерную страницу*/
            $scope.addSlide = function(){
                $scope.currentPage.content_structure.push(
                    {
                        "block" : {
                            "type" : "definitiveCard",
                            "content" : {
                                "title" : "Заголовок",
                                "second_title" : "Заголовок 2",
                                "text" : "Текст текст",
                                "clarification" : "Пояснение, пояснение"
                            },
                            "settings" : {
                                "main_image" : {
                                    "url" : null
                                },
                                "image" : {
                                    "url" : null
                                }
                            }
                        },
                        "content_structure" : []
                    }
                );
            };

            /*Функция сдвигающая слайд влево в порядке сладов*/
            $scope.slideLeft = function(slideID){
                if (slideID != 0 ){
                    var temp = $scope.currentPage.content_structure[slideID];
                    $scope.currentPage.content_structure[slideID] = $scope.currentPage.content_structure[slideID - 1];
                    $scope.currentPage.content_structure[slideID - 1] = temp;
                }
            };

            /*Функция сдвигающая слайд вправо в порядке слайдов*/
            $scope.slideRight = function(slideID){
                if (slideID != $scope.currentPage.content_structure.length - 1){
                    var temp = $scope.currentPage.content_structure[slideID];
                    $scope.currentPage.content_structure[slideID] = $scope.currentPage.content_structure[slideID + 1];
                    $scope.currentPage.content_structure[slideID + 1] = temp;
                }
            };

            /*Функция вызывающая окно настроек слайдерной страницы*/
            $scope.getSliderPageSettings = function(){
                $('#sliderPage-settings').modal('show');
            };

            /*Функция меняющая тип карточек слайдерной страницы*/
            $scope.changeCardType = function(type){
                $scope.currentPage.content_structure.forEach(function(slide){
                    slide.block.type = type;
                });
            };

            /*Функция поиска коллекции по имени*/
            $scope.getCollection = function(collection_name){
                for(var index = 0; $scope.collections.length - 1; index++){
                    if ($scope.collections[index].collection_name == collection_name) {
                        return $scope.collections[index];
                    }
                }
            };

            $scope.selectedDoc = undefined;

            /*Функция, устанавливающая выбранный документ в форме настройки комплекта документов*/
            $scope.onDocSelect = function(item){
                $scope.selectedDoc = item;
            };

            $scope.addingDoc = false;

            /*Функция добавления документа в комплект*/
            $scope.addDoc = function(block){
                block.documents.push(
                    {
                        "id" : $scope.selectedDoc.id
                    }
                );
            };

            /*Функция удаления документа из комплекта*/
            $scope.deleteDoc = function(block, docID){
                block.documents.splice(docID, 1);
            };
        }
    ]
);