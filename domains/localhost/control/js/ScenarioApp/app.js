var app = angular.module('ScenarioApp', ['ui.tree', 'ui.select', 'angular-medium-editor']);

//Регистрация CSRF-токена для запросов
app.config([
    "$httpProvider", function($httpProvider) {
        $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
        //$httpProvider.defaults.headers.post['Content-Type'] = "application/x-www-form-urlencoded; charset=UTF-8";
    }
]);

//Фабрика для передачи данных о текущих узлах между контроллерами
app.factory('$currentNodes', function(){
    var currentGroupID = null;
    var currentStepID = null;
    var currentSlideID = null;
    var status = null; //false - step, true - group
    return {
        setGroup: function(groupID){
            currentGroupID = groupID;
            status = true;
        },
        getGroup: function(){
            return currentGroupID;
        },
        setStep: function(stepID){
            currentStepID = stepID;
            status = false;
        },
        getStep: function(){
            return currentStepID;
        },
        setSlide: function(slideID){
            currentSlideID = slideID;
        },
        getSlide: function(){
            return currentSlideID;
        },
        getStatus: function(){
            return status;
        }
    }
});



app.directive("contenteditable", function() {
    return {
        restrict: "A",
        require: "ngModel",
        link: function(scope, element, attrs, ngModel) {

            function read() {
                ngModel.$setViewValue(element.html());
            }

            ngModel.$render = function() {
                element.html(ngModel.$viewValue || "");
            };

            element.bind("blur keyup change", function() {
                scope.$apply(read);
            });
        }
    };
});
