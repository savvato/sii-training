<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '../../../SII/vendor/autoload.php');
require(__DIR__ . '../../../SII/vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '../../../SII/common/config/bootstrap.php');
require(__DIR__ . '../../../SII/frontend/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '../../../SII/common/config/main.php'),
    require(__DIR__ . '../../../SII/common/config/main-local.php'),
    require(__DIR__ . '../../../SII/frontend/config/main.php'),
    require(__DIR__ . '../../../SII/frontend/config/main-local.php')
);

$application = new yii\web\Application($config);
$application->run();
