
var trigger = true;
$('#show-categories').click(function(){
    if (trigger) {
        $('#arrow').removeClass('glyphicon-chevron-down');
        $('#arrow').addClass('glyphicon-chevron-up');
        trigger = false;
    }
    else {
        $('#arrow').removeClass('glyphicon-chevron-up');
        $('#arrow').addClass('glyphicon-chevron-down');
        trigger = true;
    }
});

var trigger2 = 1;
$('.icon-menu').click(function() {
    if (trigger2 == 1){
        $('.sidebar').animate({
            left: "0px"
        }, 300);
        $('.icon-menu').animate({
            left: "280px"
        }, 300);
        $('.content').animate({
            left: "280px"
        }, 300);
        trigger2 = 0;
    }
    else {
        $('.sidebar').animate({
            left: "-280px"
        }, 300);
        $('.icon-menu').animate({
            left: "0px"
        }, 300);
        $('.content').animate({
            left: "0px"
        }, 300);
        trigger2 = 1;
    }
});
