$(document).ready(function(){
    $('.trick-main').click(function(){
        $(this).parent().find('.trick-content').height($(this).height());
        if($(this).parent().find('.trick-content').css('opacity') == 1){
            $(this).parent().find('.trick-content').animate(
                {
                    opacity: 0
                },
                200
            );
            $(this).parent().find('.trick-content').css('z-index', -1);
        }
        else {
            $(this).parent().find('.trick-content').animate(
                {
                    opacity: 1
                },
                200
            );
            $(this).parent().find('.trick-content').css('z-index', 10);
        }

    });
    $('.trick-content').each(function(indx, element){
        $(element).width($(element).parent().find('.trick-main').width());
    });
    $('.trick-content').click(function(){
        if ($(this).css('opacity') == 1){
            $(this).animate({
                    opacity: 0
                },
                200
            );
            $(this).css('z-index', -1);
        }
        else {
            $(this).animate({
                    opacity: 1
                },
                200
            );
            $(this).css('z-index', 10);
        }

    });
});